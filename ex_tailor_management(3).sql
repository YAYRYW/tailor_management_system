-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2017 at 05:05 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ex_tailor_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `ex_customer_info`
--

CREATE TABLE `ex_customer_info` (
  `customer_id` int(50) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `customer_address` varchar(100) NOT NULL,
  `customer_contact_no` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ex_customer_info`
--

INSERT INTO `ex_customer_info` (`customer_id`, `customer_name`, `customer_address`, `customer_contact_no`) VALUES
(27, 'Shah Faisal', 'Khilgaon, Dhaka', '01717506606'),
(28, 'Shah Faisal', 'Khilgaon, Dhaka', '01673601675');

-- --------------------------------------------------------

--
-- Table structure for table `ex_order_manage`
--

CREATE TABLE `ex_order_manage` (
  `id` int(50) NOT NULL,
  `order_no` varchar(100) NOT NULL,
  `order_date` varchar(50) NOT NULL,
  `customer_id` int(50) NOT NULL,
  `delivery_date` varchar(50) DEFAULT NULL,
  `total_pay` int(50) NOT NULL,
  `discount` int(100) NOT NULL,
  `paid` int(100) NOT NULL,
  `due` int(100) NOT NULL,
  `order_status` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `delivered_date` varchar(255) NOT NULL,
  `delivered_by` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ex_order_manage`
--

INSERT INTO `ex_order_manage` (`id`, `order_no`, `order_date`, `customer_id`, `delivery_date`, `total_pay`, `discount`, `paid`, `due`, `order_status`, `user_id`, `delivered_date`, `delivered_by`) VALUES
(29, '1', '24-02-2017 9:30 PM', 27, '28-02-2017 11:00 PM', 600, 0, 600, 0, '1', 4, '24-02-2017 09:26 PM', 4),
(30, '2', '24-02-2017 9:00 PM', 27, '28-02-2017 11:30 PM', 1500, 0, 1500, 0, '1', 4, '24-02-2017 09:26 PM', 4),
(31, '3', '24-02-2017 9:00 PM', 28, '24-02-2017 11:00 PM', 1600, 0, 100, 1500, '0', 4, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ex_payment_management`
--

CREATE TABLE `ex_payment_management` (
  `id` int(50) NOT NULL,
  `order_no` int(50) NOT NULL,
  `payment_date` varchar(100) NOT NULL,
  `delivery_date` varchar(100) NOT NULL,
  `customer_id` int(50) NOT NULL,
  `total_paid` int(100) NOT NULL,
  `creator_id` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ex_payment_management`
--

INSERT INTO `ex_payment_management` (`id`, `order_no`, `payment_date`, `delivery_date`, `customer_id`, `total_paid`, `creator_id`) VALUES
(30, 1, '24-02-2017 09:24 PM', '', 27, 300, 4),
(31, 2, '24-02-2017 09:25 PM', '', 27, 1000, 4),
(32, 2, '24-02-2017 09:26 PM', '', 27, 500, 4),
(33, 1, '24-02-2017 09:26 PM', '', 27, 300, 4),
(34, 3, '24-02-2017 10:13 PM', '', 28, 100, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ex_product_manage`
--

CREATE TABLE `ex_product_manage` (
  `product_id` int(50) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_minimum_price` varchar(100) NOT NULL,
  `product_regular_price` varchar(100) NOT NULL,
  `discount_%_price` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ex_product_manage`
--

INSERT INTO `ex_product_manage` (`product_id`, `product_name`, `product_minimum_price`, `product_regular_price`, `discount_%_price`) VALUES
(1, 'Shirt', '250', '300', '200'),
(2, 'Pant', '450', '500', '420'),
(3, 'Panjabi', '400', '500', '450');

-- --------------------------------------------------------

--
-- Table structure for table `ex_role`
--

CREATE TABLE `ex_role` (
  `role_id` int(50) NOT NULL,
  `role_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ex_role`
--

INSERT INTO `ex_role` (`role_id`, `role_type`) VALUES
(1, 'Admin'),
(2, 'user'),
(3, 'Master');

-- --------------------------------------------------------

--
-- Table structure for table `ex_user_manage`
--

CREATE TABLE `ex_user_manage` (
  `user_id` int(50) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_address` varchar(100) NOT NULL,
  `user_contact_no` varchar(100) NOT NULL,
  `user_role_id` int(50) NOT NULL,
  `user_password` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ex_user_manage`
--

INSERT INTO `ex_user_manage` (`user_id`, `full_name`, `user_name`, `user_address`, `user_contact_no`, `user_role_id`, `user_password`) VALUES
(4, 'Faisal', 'admin', 'Dhaka', '0154885555', 1, '601f1889667efaebb33b8c12572835da3f027f78'),
(5, 'Faisal khan', 'Shah Faisal', 'Khilgaon, Dhaka', '01717506606', 3, '601f1889667efaebb33b8c12572835da3f027f78'),
(6, 'dfgdfg', 'Jalal', 'Badda', '0154885555', 4, '601f1889667efaebb33b8c12572835da3f027f78'),
(7, 'Hamidul Islam', 'Rahat', 'Badda', '0123564789', 1, '601f1889667efaebb33b8c12572835da3f027f78'),
(8, 'Shah Khan', 'shah', 'Badda', '0154885555', 3, 'c984aed014aec7623a54f0591da07a85fd4b762d'),
(9, 'Shahedul Shuvo', 'dfgd', 'Badda', '0123456789', 2, '601f1889667efaebb33b8c12572835da3f027f78');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `order_number` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `master_name` int(11) NOT NULL,
  `measurement` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_number`, `product_id`, `quantity`, `master_name`, `measurement`, `total`) VALUES
(85, 1, 1, 2, 8, '20, 222, 48, 36, 25,98', '600'),
(86, 2, 2, 3, 8, '20, 22, 48, 36, 25', '1500'),
(87, 3, 1, 2, 5, '20, 22, 48, 36, 25', '600'),
(88, 3, 2, 2, 5, '', '1000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ex_customer_info`
--
ALTER TABLE `ex_customer_info`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `ex_order_manage`
--
ALTER TABLE `ex_order_manage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ex_payment_management`
--
ALTER TABLE `ex_payment_management`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `creator_id` (`creator_id`);

--
-- Indexes for table `ex_product_manage`
--
ALTER TABLE `ex_product_manage`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `ex_role`
--
ALTER TABLE `ex_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `ex_user_manage`
--
ALTER TABLE `ex_user_manage`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ex_customer_info`
--
ALTER TABLE `ex_customer_info`
  MODIFY `customer_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `ex_order_manage`
--
ALTER TABLE `ex_order_manage`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `ex_payment_management`
--
ALTER TABLE `ex_payment_management`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `ex_product_manage`
--
ALTER TABLE `ex_product_manage`
  MODIFY `product_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ex_role`
--
ALTER TABLE `ex_role`
  MODIFY `role_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ex_user_manage`
--
ALTER TABLE `ex_user_manage`
  MODIFY `user_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
