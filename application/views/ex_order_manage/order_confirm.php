<!-- dattimepicker css -->
<link rel="stylesheet" type="text/css"
      href="<?php echo base_url() ?>plugins/datetimepicker/jquery.datetimepicker.min.css"/>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/colorpicker/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/select2/select2.min.css">

<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div id="print">
        <section class="content-header">
            <h1>
                Confirmed Order
                <small>Detail informaiton of confirmed order</small>
            </h1>
        </section>
        <section class="content">
            <label>Customer details</label>
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Contact No</th>
                            <th>Customer Address</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $orderN = $on;
                        $customer = $this->Ex_order_manage_model->get_customer_by_order_no($orderN); ?>

                        <tr>
                            <td><?php echo $customer['customer_name'] ?></td>
                            <td><?php echo $customer['customer_contact_no'] ?></td>
                            <td><?php echo $customer['customer_address'] ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <label>Order details</label>
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Order Status</th>
                            <th>Order Date</th>
                            <th>Delivery Date</th>
                            <th>Order Received By</th>
                            <th>Order Delivered By</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($order as $d) { ?>
                            <tr>
                                <td><?php echo $d['order_no'] ?></td>
                                <input type="text" value="<?php echo $d['order_no'] ?>" name="order_no" hidden>
                                <td><?php $status = $d['order_status'];
                                    if ($status == 0) { ?>
                                        <span class="label label-danger">Pending</span>
                                    <?php } else if ($status == 1) { ?>
                                        <span class="label label-success">Delivered</span>
                                    <?Php } ?></td>
                                <td><?php echo $d['order_date'] ?></td>
                                <td><?php echo $d['delivered_date'] ?></td>

                                <td><?php echo $d['full_name'] ?></td>
                                <td><?php $user_id = $d['delivered_by'];

                                    $name = $this->Ex_user_manage_model->get_ex_user_manage($user_id);
                                    echo $name['full_name'];

                                    ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Product Name</th>
                            <th>Product Quantity</th>
                            <th>Measurement</th>
                            <th>Master Name</th>
                            <th>Unit Price</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $sl = 1;
                        foreach ($order_details as $ol) {
                            $sln = $sl++; ?>
                            <tr>
                                <td><?php echo $sln; ?></td>
                                <td><?php echo $ol['product_name']; ?></td>
                                <td><?php echo $ol['quantity']; ?></td>
                                <td><?php echo $ol['measurement']; ?></td>
                                <td><?php $user_id = $ol['master_name'];
                                    $master = $this->Ex_user_manage_model->get_ex_user_manage($user_id);
                                    echo $master['full_name']; ?></td>
                                <td><?php echo $ol['total']; ?></td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
            <label>Payment details</label>
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Total Price</th>
                            <th>Paid Amount</th>
                            <th>Due Payment</th>
                            <th>Discount (%)</th>
                            <th>Price After Discount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($payment as $pd) { ?>
                            <tr>
                                <td><?php echo $pd['total_pay']; ?></td>
                                <td><?php echo $pd['paid']; ?>
                                    <input type="text" value="<?php echo $d['order_no'] ?>" name="paid" hidden>
                                </td>
                                <td><?php echo $pd['due']; ?></td>
                                <td><?php echo $pd['discount']; ?></td>
                                <td><?php $total = $pd['total_pay'];
                                    $dis = $pd['discount'];
                                    $net = ($total - $dis);
                                    echo $net; ?>
                                    <input type="text" value="<?php echo $net ?>" name="paid" hidden>
                                    <input type="text" value="0" name="due" hidden>
                                    <input type="text" value="<?php echo $pd['due']; ?>" name="total_paid" hidden>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
    <div class="text-right">
        <button type="button" class="btn btn-success" id="btnPrint" onclick="myFunction()">Print</button>
        <button type="button" class="btn btn-success bg-red"
                onclick="window.location='<?php echo base_url('ex_order_manage/index'); ?>';">Close
        </button>
    </div>
</div>
</section>

<!-- /.content-wrapper -->

<script>
    function myFunction() {
        window.print();
    }
</script>