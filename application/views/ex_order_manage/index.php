<script>
    $(function () {
        $('#orderTable').DataTable({
            "sDom": '<"top`"ipf>rt<"bottom"flp><"clear">',
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "autoWidth": true
        });
    });
</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"> <?php $this->load->view('/flashMessage'); ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Orders
            <small>All Orders</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="orderTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Status</th>
                                <th>Order Item</th>
                                <th>Customer Name</th>
                                <th>Contact</th>
                                <th>Order Date &amp; Time</th>
                                <th>Delivery Date &amp; Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($ex_order_manage as $e) { ?>
                                <tr>

                                    <td>
                                        <?php $status = $e['order_status'];
                                        if ($status == 0) { ?>
                                            <a href="<?php echo base_url() ?>Ex_order_manage/view_indevidual_order/<?php echo $e['order_no'] ?>"><?php echo $e['order_no']; ?></a>
                                        <?php } else if ($status == 1) { ?>
                                            <a href="<?php echo base_url() ?>Ex_order_manage/order_confirm/<?php echo $e['order_no'] ?>"><?php echo $e['order_no']; ?></a>
                                        <?Php } ?>
                                    </td>
                                    <td><?php $status = $e['order_status'];
                                        if ($status == 0) { ?>
                                            <span class="label label-danger">Pending</span>
                                        <?php } else if ($status == 1) { ?>
                                            <span class="label label-success">Delivered</span>
                                        <?Php } ?>

                                    </td>
                                    <td>
                                        <?php
                                        $this->load->model('Ex_order_manage_model');
                                        $order_no = $e['order_no'];
                                        $product = $this->Ex_order_manage_model->view_product_by_order_id($order_no);
                                        foreach ($product as $p) {
                                            echo $p['product_name'] . " , ";
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $e['customer_name']; ?></td>
                                    <td><?php echo $e['customer_contact_no']; ?></td>
                                    <td><?php echo $e['order_date']; ?></td>
                                    <td><?php echo $e['delivery_date']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                            <!-- <tfoot>
                            <tr>
                              <th>Rendering engine</th>
                              <th>Browser</th>
                              <th>Status</th>
                              <th>Engine version</th>
                              <th>CSS grade</th>
                            </tr>
                            </tfoot> -->
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->