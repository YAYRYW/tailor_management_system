<style>
    .marginbottom {
        margin-bottom: 5px;
    }
</style>
<div class="col-md-2"></div>
<div class="col-md-9">
    <?php $this->load->view('/flashMessage'); ?>


    <table class="table table-bordered table-striped table-hover table-condensed table-responsive text-center">
        <tr>

            <th class="text-center">Order No</th>
            <th class="text-center">Order Date</th>
            <th class="text-center">Customer Id</th>
            <th class="text-center">Product</th>
            <th class="text-center"> Measurement</th>
            <th class="text-center">Master</th>
            <th class="text-center">Delivery Date</th>
            <th class="text-center">Total</th>
            <th class="text-center">Status</th>
            <th class="text-center">Actions</th>
        </tr>
        <?php foreach ($ex_order_manage as $e) { ?>
            <tr>

                <td><?php echo $e['order_no']; ?></td>
                <td><?php echo $e['order_date']; ?></td>
                <td><?php echo $e['customer_id']; ?></td>
                <td><?php echo $e['product_name']; ?></td>
                <td><?php echo $e['customer_measurement']; ?></td>
                <td><?php echo $e['user_name']; ?></td>
                <td><?php echo $e['delivery_date']; ?></td>
                <td><?php echo $e['total_pay']; ?></td>
                <td><?php $status = $e['order_status'];
                    if ($status == 0) {
                        echo "<span class='text-primary'>" . "Pending" . "</span>";
                    } else if ($status == 1) {
                        echo "<span class='text-danger'>" . "Delivered" . "</span>";;
                    }
                    ?></td>
                <td>
                    <?php $status = $e['order_status'];
                    if ($status == 0) { ?>
                        <a href="<?php echo site_url('ex_order_manage/edit/' . $e['id']); ?>"
                           class="btn btn-xs btn-info marginbottom">Edit</a><br>
                    <?php } ?>
                    <a href="<?php echo site_url('ex_order_manage/remove/' . $e['id']); ?>"
                       class="btn btn-xs btn-danger">Delete</a>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>
<div class="col-md-1"></div>