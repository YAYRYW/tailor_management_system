<!-- dattimepicker css -->
<link rel="stylesheet" type="text/css"
      href="<?php echo base_url() ?>plugins/datetimepicker/jquery.datetimepicker.min.css"/>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/colorpicker/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/select2/select2.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Order Details
            <small>Detail informaiton of confirmed order</small>
        </h1>
    </section>

    <section class="content">
        <label>Customer details</label>
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Customer Name</th>
                        <th>Contact No</th>
                        <th>Customer Address</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $orderN = $on;
                    $customer = $this->Ex_order_manage_model->get_customer_by_order_no($orderN); ?>
                    <?php echo form_open('ex_order_manage/edit/' . $orderN); ?>
                    <tr>
                        <input type="text" value="<?php echo $customer['customer_id']; ?>" name="customer_id" hidden>
                        <td><?php echo $customer['customer_name'] ?></td>
                        <td><?php echo $customer['customer_contact_no'] ?></td>
                        <td><?php echo $customer['customer_address'] ?></td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <label>Order details</label>
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Order ID</th>
                        <th>Order Status</th>
                        <th>Order Date</th>
                        <th>Delivery Date</th>
                        <th>New Delivery Date</th>
                        <th>Order Received By</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($order as $d) { ?>
                        <tr>
                            <td><?php echo $d['order_no'] ?></td>
                            <input type="text" value="<?php echo $d['order_no'] ?>" name="order_no" hidden>
                            <td><?php $status = $d['order_status'];
                                if ($status == 0) { ?>
                                    <span class="label label-danger">Pending</span>
                                <?php } else if ($status == 1) { ?>
                                    <span class="label label-success">Delivered</span>
                                <?Php } ?></td>
                            <td><?php echo $d['order_date'] ?></td>
                            <td><?php echo $d['delivery_date'] ?></td>
                            <td>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i>
                                            <input type="checkbox"
                                                   onclick="var input = document.getElementById('newDeliveryDate'); if(this.checked){ input.disabled = false; input.focus();}else{input.disabled=true;input.value=''}">
                                        </i>
                                    </div>
                                    <input type="text" name="deliveryDate" class="form-control pull-right"
                                           id="newDeliveryDate" disabled="disabled">
                                </div>
                            </td>
                            <td><?php echo $d['full_name'] ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>

                    <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>Product Quantity</th>
                        <th>Measurement</th>
                        <th>Master Name</th>
                        <th>Unit Price</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $sl = 1;
                    foreach ($order_details as $ol) {
                        $sln = $sl++; ?>
                        <tr>
                            <td><?php echo $sln; ?></td>
                            <td><?php echo $ol['product_name']; ?></td>
                            <td><?php echo $ol['quantity']; ?></td>
                            <td><input type="text" class="form-control" placeholder="Customer Measurment Here"
                                       value="<?php echo $ol['measurement']; ?>"/></td>
                            <td><?php $user_id = $ol['master_name'];
                                $master = $this->Ex_user_manage_model->get_ex_user_manage($user_id);
                                echo $master['full_name']; ?></td>
                            <td><?php echo $ol['total']; ?></td>
                        </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
        <label>Payment details</label>
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Total Price</th>
                        <th>Advance Payment</th>
                        <th>Due Payment</th>
                        <th>Discount (%)</th>
                        <th>Price After Discount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($payment as $pd) { ?>
                        <tr>
                            <td><?php echo $pd['total_pay']; ?></td>
                            <td><?php echo $pd['paid']; ?>
                                <input type="text" value="<?php echo $d['order_no'] ?>" name="paid" hidden>
                            </td>
                            <td><?php echo $pd['due']; ?></td>
                            <td><?php echo $pd['discount']; ?></td>
                            <td><?php $total = $pd['total_pay'];
                                $dis = $pd['discount'];
                                $net = ($total - $dis);
                                echo $net; ?>
                                <input type="text" value="<?php echo $net ?>" name="paid" hidden>
                                <input type="text" value="0" name="due" hidden>
                                <input type="text" value="<?php echo $pd['due']; ?>" name="total_paid" hidden>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-success" >Confirm
                Order
            </button>
            <button type="button" id="save" class="btn btn-success bg-blue">Save Order</button>
            <button type="button" class="btn btn-success bg-orange" onclick="myFunction()">Print Order</button>
            <button type="button" class="btn btn-success bg-red"
                    onclick="window.location='<?php echo base_url('ex_order_manage/index'); ?>';">Close
            </button>
        </div>
        <?php form_close(); ?>
    </section>
</div>
<!-- /.content-wrapper -->
<script>
    $('#newDeliveryDate').datetimepicker({
        format: 'd-m-Y g:i A',
        minDate: 0,
        step: 30
    });
</script>

<script>
    function myFunction() {
        window.print();
    }
</script>

<script>
    $("#save").click(function () {


        var selected = $("#newDeliveryDate").val();
        dat = 'newDeliveryDate=' + selected;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url("Ex_order_manage/update");?>",
            data: dat,
            success: function (data) {
                console.log(data);
                if (data == 'fail') {
                    alert("there is no data");
                }
                else {
                    alert(data);
                }
            }
        });


    });
</script>


