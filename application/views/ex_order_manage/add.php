<!-- dattimepicker css -->
<link rel="stylesheet" type="text/css"
      href="<?php echo base_url() ?>plugins/datetimepicker/jquery.datetimepicker.min.css"/>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/iCheck/all.css">
<!-- Bootstrap Color Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/colorpicker/bootstrap-colorpicker.min.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/timepicker/bootstrap-timepicker.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url() ?>plugins/select2/select2.min.css">


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            New Order
            <small>Create a new order</small>
        </h1>
    </section>
    <!-- Main content -->
    <?php $this->load->view('/flashMessage'); ?>
    <?php echo form_open('ex_order_manage/add', ['id' => 'add_order']); ?>

    <section class="content"
             style="padding-top: 0px !important; padding-bottom: 0px !important; min-height: 10px !important;">
        <table id="newOrderTable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Customer Contact No</th>
                <th>Customer Name</th>
                <th>Customer Address</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <input type="text" class="form-control" placeholder="Customer Contact No" name="customer_contact_no" id="customer_contact_no"
                           required>
                </td>
                <td>
                    <input type="text" class="form-control" placeholder="Customer Name" name="customer_name" id="customer_name" required>
                </td>
                <td>
                    <input type="text" class="form-control" placeholder="Customer Address" name="customer_address" id="customer_address">
                </td>
            </tr>
            </tbody>
        </table>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Order ID</th>
                <th>Order Date</th>
                <th>Delivery Date</th>
                <th>Order Received By</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <input type="text" class="form-control" placeholder="Order ID" name="order_no"
                           value="<?php print_r($order_no['order_no'] + 1); ?>" required>
                </td>
                <td>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="orderDate" name="order_date" required>

                    </div>
                </td>
                <td>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="deliveryDate" name="delivery_date"
                               required>
                    </div>
                </td>
                <td>
                    <input type="text" class="form-control" value="<?php echo $this->session->userdata('full_name'); ?>"
                           disabled="disable" name="user_id">
                </td>
            </tr>
            </tbody>
        </table>

    </section>
    <section class="content"
             style="padding-top: 0px !important; padding-bottom: 0px !important; min-height: 10px !important;">
        <table class="table table-bordered" style="margin-bottom: 0px !important;">
            <tbody>
            <tr>
                <td style="padding: 0px !important;">
                    <label>Order details</label>
                </td>
                <td style="padding: 0px !important;">
                </td>
            </tr>
            </tbody>
        </table>
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Product Name</th>
                        <th>Measurement</th>
                        <th>Master Name</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th>Sub Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <input type="text" value="1" class="form-control" disabled>
                        </td>
                        <td>

                            <select class="form-control form-control selectpicker " id="product"
                                    name="product_id[]" required>
                                <option value="">select</option>
                                <?php

                                foreach ($all_ex_product_manage as $ex_product_manage) {
                                    $selected = ($ex_product_manage['product_id'] == $this->input->post('product_id')) ? ' selected="selected"' : "";

                                    echo '<option value="' . $ex_product_manage['product_id'] . '" ' . $selected . '>' . $ex_product_manage['product_name'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <script type="text/javascript">
                            $("#product").change(function () {
                                var selected = $("#product").val();
                                dat = 'product_id=' + selected;
                                var url = '<?php echo site_url("ex_product_manage/get_product_data_by_product_id");?>';
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    data: dat,
                                    success: function (data) {
                                        console.log(data);
                                        if (data == 'fail') {
                                            alert("there is no data");
                                        }
                                        else {
                                            $("#qty").keyup(function () {
                                                var quantity = $('#qty').val();
                                                var price = data;
                                                var total = (quantity * price);
                                                $('#price').val(data);
                                                $('#subtotal').val(total);
                                                var sub = parseInt($('#subtotal').val());
                                                $('#tp').val(total);


                                            });
                                        }
                                    }
                                });
                            });

                        </script>
                        <td>
                            <input type="text" class='form-control' placeholder="Customer Measurment Here"
                                   name="measurement[]">
                        </td>
                        <td>
                            <select class="form-control form-control selectpicker" name="master_name[]" required>
                                <option value="">select</option>
                                <?php
                                foreach ($all_ex_user_manage as $ex_user_manage) {
                                    $selected = ($ex_user_manage['user_id'] == $this->input->post('master_name')) ? ' selected="selected"' : "";

                                    echo '<option value="' . $ex_user_manage['user_id'] . '" ' . $selected . '>' . $ex_user_manage['user_name'] . '</option>';
                                }
                                ?>
                            </select>

                        </td>
                        <td>
                            <input type="text" class="form-control" placeholder="Order Quantity" id="qty"
                                   name="quantity[]" required>
                        </td>
                        <td>
                            <input type="text" class="form-control" disabled value="0" id="price">
                        </td>
                        <td>
                            <input type="text" class="form-control" value="" id="subtotal" name="total[]" readonly
                                   required>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <input type="text" value="2" class="form-control" disabled>
                        </td>
                        <td>
                            <select class="form-control form-control selectpicker" id="product1" name="product_id[]">
                                <option value="">select</option>
                                <?php
                                foreach ($all_ex_product_manage as $ex_product_manage) {
                                    $selected = ($ex_product_manage['product_id'] == $this->input->post('product_id')) ? ' selected="selected"' : "";

                                    echo '<option value="' . $ex_product_manage['product_id'] . '" ' . $selected . '>' . $ex_product_manage['product_name'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="text" class='form-control' placeholder="Customer Measurment Here"
                                   name="measurement[]">
                        </td>
                        <td>
                            <select class="form-control form-control selectpicker" name="master_name[]">
                                <option value="">select</option>
                                <?php
                                foreach ($all_ex_user_manage as $ex_user_manage) {
                                    $selected = ($ex_user_manage['user_id'] == $this->input->post('master_name')) ? ' selected="selected"' : "";

                                    echo '<option value="' . $ex_user_manage['user_id'] . '" ' . $selected . '>' . $ex_user_manage['user_name'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="text" class="form-control" placeholder="Order Quantity" id="qty1"
                                   name="quantity[]">
                        </td>
                        <td>
                            <input type="text" class="form-control" disabled value="0" id="price1">
                        </td>
                        <td>
                            <input type="text" class="form-control" value="" id="subtotal1" name="total[]" readonly>
                        </td>
                    </tr>
                    <script type="text/javascript">
                        $("#product1").change(function () {
                            var selected = $("#product1").val();
                            dat = 'product_id=' + selected;
                            var url = '<?php echo site_url("ex_product_manage/get_product_data_by_product_id");?>';
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: dat,
                                success: function (data) {
                                    console.log(data);
                                    if (data == 'fail') {
                                        alert("there is no data");
                                    }
                                    else {
                                        $("#qty1").keyup(function () {
                                            var quantity = $('#qty1').val();
                                            var price = data;
                                            var total = (quantity * price);
                                            $('#price1').val(data);
                                            $('#subtotal1').val(total);
                                            var sub = parseInt($('#subtotal').val());
                                            var sub1 = parseInt($('#subtotal1').val());
                                            var total1 = parseInt(sub + sub1);
                                            $('#tp').val(total1);


                                        });
                                    }
                                }
                            });
                        });

                    </script>
                    <tr>
                        <td>
                            <input type="text" value="3" class="form-control" disabled>
                        </td>
                        <td>
                            <select class="form-control form-control selectpicker" id="product2" name="product_id[]">
                                <option value="">select</option>
                                <?php
                                foreach ($all_ex_product_manage as $ex_product_manage) {
                                    $selected = ($ex_product_manage['product_id'] == $this->input->post('product_id')) ? ' selected="selected"' : "";

                                    echo '<option value="' . $ex_product_manage['product_id'] . '" ' . $selected . '>' . $ex_product_manage['product_name'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="text" class='form-control' placeholder="Customer Measurment Here"
                                   name="measurement[]">
                        </td>
                        <td>
                            <select class="form-control form-control selectpicker" name="master_name[]">
                                <option value="">select</option>
                                <?php
                                foreach ($all_ex_user_manage as $ex_user_manage) {
                                    $selected = ($ex_user_manage['user_id'] == $this->input->post('master_name')) ? ' selected="selected"' : "";

                                    echo '<option value="' . $ex_user_manage['user_id'] . '" ' . $selected . '>' . $ex_user_manage['user_name'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="text" class="form-control" placeholder="Order Quantity" id="qty2"
                                   name="quantity[]">
                        </td>
                        <td>
                            <input type="text" class="form-control" disabled value="0" id="price2">
                        </td>
                        <td>
                            <input type="text" class="form-control" value="" id="subtotal2" name="total[]" readonly>
                        </td>
                    </tr>
                    <script type="text/javascript">
                        $("#product2").change(function () {
                            var selected = $("#product2").val();
                            dat = 'product_id=' + selected;
                            var url = '<?php echo site_url("ex_product_manage/get_product_data_by_product_id");?>';
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: dat,
                                success: function (data) {
                                    console.log(data);
                                    if (data == 'fail') {
                                        alert("there is no data");
                                    }
                                    else {
                                        $("#qty2").keyup(function () {
                                            var quantity = $('#qty2').val();
                                            var price = data;
                                            var total = (quantity * price);
                                            $('#price2').val(data);
                                            $('#subtotal2').val(total);
                                            var sub = parseInt($('#subtotal').val());
                                            var sub1 = parseInt($('#subtotal1').val());
                                            var sub2 = parseInt($('#subtotal2').val());
                                            var total1 = parseInt(sub + sub1 + sub2);
                                            $('#tp').val(total1);

                                        });
                                    }
                                }
                            });
                        });

                    </script>
                    <tr>
                        <td>
                            <input type="text" value="4" class="form-control" disabled>
                        </td>
                        <td>
                            <select class="form-control form-control selectpicker" id="product3" name="product_id[]">
                                <option value="">select</option>
                                <?php
                                foreach ($all_ex_product_manage as $ex_product_manage) {
                                    $selected = ($ex_product_manage['product_id'] == $this->input->post('product_id')) ? ' selected="selected"' : "";

                                    echo '<option value="' . $ex_product_manage['product_id'] . '" ' . $selected . '>' . $ex_product_manage['product_name'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="text" class='form-control' placeholder="Customer Measurment Here"
                                   name="measurement[]">
                        </td>
                        <td>
                            <select class="form-control form-control selectpicker" name="master_name[]">
                                <option value="">select</option>
                                <?php
                                foreach ($all_ex_user_manage as $ex_user_manage) {
                                    $selected = ($ex_user_manage['user_id'] == $this->input->post('master_name')) ? ' selected="selected"' : "";

                                    echo '<option value="' . $ex_user_manage['user_id'] . '" ' . $selected . '>' . $ex_user_manage['user_name'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="text" class="form-control" placeholder="Order Quantity" id="qty3"
                                   name="quantity[]">
                        </td>
                        <td>
                            <input type="text" class="form-control" disabled value="0" id="price3">
                        </td>
                        <td>
                            <input type="text" class="form-control" value="" id="subtotal3" name="total[]" readonly>
                        </td>
                    </tr>
                    <script type="text/javascript">
                        $("#product3").change(function () {
                            var selected = $("#product3").val();
                            dat = 'product_id=' + selected;
                            var url = '<?php echo site_url("ex_product_manage/get_product_data_by_product_id");?>';
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: dat,
                                success: function (data) {
                                    console.log(data);
                                    if (data == 'fail') {
                                        alert("there is no data");
                                    }
                                    else {
                                        $("#qty3").keyup(function () {
                                            var quantity = $('#qty3').val();
                                            var price = data;
                                            var total = (quantity * price);
                                            $('#price3').val(data);
                                            $('#subtotal3').val(total);
                                            var sub = parseInt($('#subtotal').val());
                                            var sub1 = parseInt($('#subtotal1').val());
                                            var sub2 = parseInt($('#subtotal2').val());
                                            var sub3 = parseInt($('#subtotal3').val());
                                            var total1 = parseInt(sub + sub1 + sub2 + sub3);
                                            $('#tp').val(total1);

                                        });
                                    }
                                }
                            });
                        });

                    </script>
                    <tr>
                        <td>
                            <input type="text" value="5" class="form-control" disabled>
                        </td>
                        <td>
                            <select class="form-control form-control selectpicker" id="product4" name="product_id[]">
                                <option value="">select</option>
                                <?php
                                foreach ($all_ex_product_manage as $ex_product_manage) {
                                    $selected = ($ex_product_manage['product_id'] == $this->input->post('product_id')) ? ' selected="selected"' : "";

                                    echo '<option value="' . $ex_product_manage['product_id'] . '" ' . $selected . '>' . $ex_product_manage['product_name'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="text" class='form-control' placeholder="Customer Measurment Here"
                                   name="measurement[]">
                        </td>
                        <td>
                            <select class="form-control form-control selectpicker" name="master_name[]">
                                <option value="">select</option>
                                <?php
                                foreach ($all_ex_user_manage as $ex_user_manage) {
                                    $selected = ($ex_user_manage['user_id'] == $this->input->post('master_name')) ? ' selected="selected"' : "";

                                    echo '<option value="' . $ex_user_manage['user_id'] . '" ' . $selected . '>' . $ex_user_manage['user_name'] . '</option>';
                                }
                                ?>
                            </select>
                        </td>
                        <td>
                            <input type="text" class="form-control" placeholder="Order Quantity" id="qty4"
                                   name="quantity[]">
                        </td>
                        <td>
                            <input type="text" class="form-control" disabled value="0" id="price4">
                        </td>
                        <td>
                            <input type="text" class="form-control" value="" id="subtotal4" name="total[]" readonly>
                        </td>
                    </tr>
                    <script type="text/javascript">
                        $("#product4").change(function () {
                            var selected = $("#product4").val();
                            dat = 'product_id=' + selected;
                            var url = '<?php echo site_url("ex_product_manage/get_product_data_by_product_id");?>';
                            $.ajax({
                                type: "POST",
                                url: url,
                                data: dat,
                                success: function (data) {
                                    console.log(data);
                                    if (data == 'fail') {
                                        alert("there is no data");
                                    }
                                    else {
                                        $("#qty4").keyup(function () {
                                            var quantity = $('#qty4').val();
                                            var price = data;
                                            var total = (quantity * price);
                                            $('#price4').val(data);
                                            $('#subtotal4').val(total);
                                            var sub = parseInt($('#subtotal').val());
                                            var sub1 = parseInt($('#subtotal1').val());
                                            var sub2 = parseInt($('#subtotal2').val());
                                            var sub3 = parseInt($('#subtotal3').val());
                                            var sub4 = parseInt($('#subtotal4').val());
                                            var total1 = parseInt(sub + sub1 + sub2 + sub3 + sub4);
                                            $('#tp').val(total1);

                                        });
                                    }
                                }
                            });
                        });

                    </script>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
    <section class="content" style="padding-top: 0px !important; padding-bottom: 0px !important;">
        <table class="table table-bordered" style="margin-bottom: 0px !important;">
            <tbody>
            <tr>
                <td style="padding: 0px !important;">
                    <label>Payment details</label>
                </td>
            </tr>
            </tbody>
        </table>
        <table id="paymentTable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Total Payment</th>
                <th>Discount (BDT)</th>
                <th>Advance Payment</th>
                <th>Due Payment</th>
            </tr>
            </thead>

            <tbody>

            <tr>
                <td>
                    <input type="text" value="0" class="form-control" id="tp" name="total_amount" readonly>
                </td>

                <td>
                    <input type="text" value="0" class="form-control" id="discount" name="discount">
                </td>
                <td>
                    <input type="text" value="0" class="form-control" id="advance" name="advance" required>
                </td>
                <td>
                    <input type="text" value="0" class="form-control" id="due" name="due" readonly>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="text-right">
            <span class="error"><?php echo validation_errors(); ?></span>
            <button type="submit" class="btn btn-success">Create Order</button>
            <button type="button" class="btn btn-success bg-red"
                    onClick="window.location='<?php echo site_url("Home_controller/index"); ?>'">Cancel
            </button>
        </div>
        <?php echo form_close(); ?>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    $('#orderDate').datetimepicker({
        format: 'd-m-Y g:i A',
        step: 30
    });
    $('#deliveryDate').datetimepicker({
        format: 'd-m-Y g:i A',
        step: 30
    });
</script>
<script>
    $("#advance").keyup(function () {

        var tp = parseInt($('#tp').val());
        var discount = parseInt($('#discount').val());
        var adv = parseInt($('#advance').val());
        var due = parseInt(tp - (discount + adv));
        $('#due').val(due);

    });
</script>
<script>

    $.validator.addMethod("valueNotEquals", function (value, element, arg) {
        return arg != value;
    }, "Value must not equal arg.");

    $("#add_order").validate({
        rules: {
            order_no: {
                remote: {
                    url: "<?php echo site_url("ex_payment_management/getUniqueOrderNo");?>",
                    type: "post",
                    dataType: "text",
                    data: {
                        order_no: function () {
                            return $('#add_order :input[name="order_no"]').val();
                        }
                    }

                }
            },

        },
        messages: {


            order_no: {
                remote: jQuery.validator.format("{0} is already taken.")

            },


        }


    });


</script>

<script>
    $(document).ready(function () {
        $('#customer_contact_no').autocomplete({
            source: function (request, response) {
                //console.log(request);
                $.ajax({
                    url: '<?php echo site_url("Ex_customer_info/get_customer_data");?>',
                    data: {'customer_contact_no': request.term},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {

                        response($.map(data, function (item) {

                            return {
                                label: item.customer_contact_no+ " " + item.customer_name,
                                value: item.customer_contact_no,
                                data: item
                            };
                        }));
                    }
                });
            },
            autoFocus: true,
            minLength: 0,
            select: function (event, ui) {
                var names = ui.item.data;
                var customer_contact_no = names.customer_contact_no;
                $('#customer_name').val(names.customer_name);
                $('#customer_address').val(names.customer_address);



            }
        });
    });
</script>