<?php echo form_open('ex_order_manage/edit/' . $ex_order_manage['id'], ['id' => 'edit_order', 'class' => 'form-horizontal']); ?>

<div class="container-fluid b">

    <div class="row b">
        <div class="col-md-2 b">
        </div>
        <div class="col-md-8 b">

            <div class="row">
                <div class="col-md-6 b">
                    <div class="form-group">
                        <label for="order_no" class="col-md-7 control-label">Order No<sup>*</sup></label>
                        <div class="col-md-5">
                            <input type="text" name="order_no"
                                   value="<?php echo($this->input->post('order_no') ? $this->input->post('order_no') : $ex_order_manage['order_no']); ?>"
                                   class="form-control" id="order_no" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="product_id" class="col-md-7 control-label">Product Type<sup>*</sup></label>
                        <div class="col-md-5">
                            <select name="product_id" class="form-control">
                                <option value="">select</option>
                                <?php
                                foreach ($all_ex_product_manage as $ex_product_manage) {
                                    $selected = ($ex_product_manage['product_id'] == $ex_order_manage['product_id']) ? ' selected="selected"' : null;

                                    echo '<option value="' . $ex_product_manage['product_id'] . '" ' . $selected . '>' . $ex_product_manage['product_name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="master_name" class="col-md-7 control-label">Master Name<sup>*</sup></label>
                        <div class="col-md-5">
                            <select name="master_name" class="form-control">
                                <option value="">select ex_user_manage</option>
                                <?php
                                foreach ($all_ex_user_manage as $ex_user_manage) {
                                    $selected = ($ex_user_manage['user_id'] == $ex_order_manage['master_name']) ? ' selected="selected"' : null;

                                    echo '<option value="' . $ex_user_manage['user_id'] . '" ' . $selected . '>' . $ex_user_manage['user_name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                </div>
                <div class="col-md-6 b">
                    <div class="form-group">
                        <label for="order_date" class="col-md-7 control-label">Order Date<sup>*</sup></label>
                        <div class="col-md-5">
                            <input type="text" name="order_date"
                                   value="<?php echo($this->input->post('order_date') ? $this->input->post('order_date') : $ex_order_manage['order_date']); ?>"
                                   class="form-control" id="order_date"/>
                        </div>
                    </div>
                    <script>
                        $(function () {
                            $("#order_date").datepicker({
                                dateFormat: "dd-mm-yy",
                            }).datepicker("setDate", "0");
                        });
                    </script>


                    <div class="form-group">
                        <label for="order_date" class="col-md-7 control-label">Quantity<sup>*</sup></label>
                        <div class="col-md-5">
                            <input type="text" id="total" hidden/>
                            <input type="text" name="quantity"
                                   value="<?php echo($this->input->post('quantity') ? $this->input->post('quantity') : $ex_order_manage['quantity']); ?>"
                                   class="form-control" id="quantity" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="total_pay" class="col-md-7 control-label">Total<sup>*</sup> </label>
                        <div class="col-md-5">
                            <input type="text" name="total_pay"
                                   value="<?php echo($this->input->post('total_pay') ? $this->input->post('total_pay') : $ex_order_manage['total_pay']); ?>"
                                   class="form-control" id="total_pay"/>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row ">
                <div class="col-md-12 b">
                    <div class="form-inline">
                        <p style="text-align:center;font-weight: bold">Customer Measurement<sup>*</sup></p>
                        <div class="col-md-3"></div>
                        <div class="col-md-9">
                            <textarea name="customer_measurement" class="form-control" id="customer_measurement"
                                      style="min-width: 100%"
                                      required><?php echo($this->input->post('customer_measurement') ? $this->input->post('customer_measurement') : $ex_order_manage['customer_measurement']); ?></textarea>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-12 b">
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-12">
                        <?php echo validation_errors(); ?>
                        <button type="submit" style="width: 40%;margin-top: 15px" class="btn btn-success">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="col-md-2 b">
        </div>
    </div>
</div>


<?php echo form_close(); ?>

<script>
    $("#edit_order").validate({});
</script>

