<div class="col-md-2"></div>
<div class="col-md-8">
    <?php $this->load->view('/flashMessage'); ?>
    <?php echo form_open('ex_payment_management/edit/' . $ex_payment_management['id'], ['id' => 'edit_payment', 'class' => 'form-horizontal']); ?>

    <div class="form-group">
        <label for="receipt_no" class="col-md-4 control-label">Receipt No</label>
        <div class="col-md-4">
            <input type="text" name="receipt_no"
                   value="<?php echo($this->input->post('receipt_no') ? $this->input->post('receipt_no') : $ex_payment_management['receipt_no']); ?>"
                   class="form-control" id="receipt_no"/>
        </div>
    </div>
    <div class="form-group">
        <label for="customer_id" class="col-md-4 control-label">Customer Id</label>
        <div class="col-md-4">
            <input type="text" name="customer_id"
                   value="<?php echo($this->input->post('customer_id') ? $this->input->post('customer_id') : $ex_payment_management['customer_id']); ?>"
                   class="form-control" id="customer_id"/>
        </div>
    </div>
    <div class="form-group">
        <label for="total_amount" class="col-md-4 control-label">Total Amount</label>
        <div class="col-md-4">
            <input type="text" name="total_amount"
                   value="<?php echo($this->input->post('total_amount') ? $this->input->post('total_amount') : $ex_payment_management['total_amount']); ?>"
                   class="form-control" id="total_amount"/>
        </div>
    </div>
    <div class="form-group">
        <label for="discount" class="col-md-4 control-label">Discount</label>
        <div class="col-md-4">
            <input type="text" name="discount"
                   value="<?php echo($this->input->post('discount') ? $this->input->post('discount') : $ex_payment_management['discount']); ?>"
                   class="form-control" id="discount"/>
        </div>
    </div>
    <div class="form-group">
        <label for="advance" class="col-md-4 control-label">Paid</label>
        <div class="col-md-4">
            <input type="text" name="advance"
                   value="<?php echo($this->input->post('advance') ? $this->input->post('advance') : $ex_payment_management['advance']); ?>"
                   class="form-control" id="advance" required/>
        </div>
    </div>
    <div class="form-group">
        <label for="due" class="col-md-4 control-label">Due</label>
        <div class="col-md-4">
            <input type="text" name="due"
                   value="<?php echo($this->input->post('due') ? $this->input->post('due') : $ex_payment_management['due']); ?>"
                   class="form-control" id="due" required/>
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-8">
            <?php echo validation_errors(); ?>
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </div>

    <?php echo form_close(); ?>
</div>
<div class="col-md-2"></div>


<script>
    $("#edit_payment").validate({});
</script>


