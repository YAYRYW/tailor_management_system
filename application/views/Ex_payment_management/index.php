<div class="col-md-2"></div>
<div class="col-md-9">
    <?php $this->load->view('/flashMessage'); ?>

    <table class="table table-striped table-bordered text-center">
        <tr>

            <th class="text-center">Receipt No</th>
            <th class="text-center">Customer Id</th>
            <th class="text-center">Total Amount</th>
            <th class="text-center">Paid</th>
            <th class="text-center">Due</th>
            <th class="text-center">Payment Date</th>
            <th class="text-center">Status</th>
            <th class="text-center">Actions</th>
        </tr>
        <?php foreach ($ex_payment_management as $e) { ?>
            <tr>

                <td><?php echo $e['receipt_no']; ?></td>
                <td><?php echo $e['customer_id']; ?></td>
                <td><?php echo $e['total_amount']; ?></td>
                <td><?php echo $e['advance']; ?></td>
                <td><?php echo $e['due']; ?></td>
                <td><?php echo $e['payment_date']; ?></td>
                <td><?php $status = $e['order_status'];
                    if ($status == 0) {
                        echo "<span class='text-primary'>" . "Pending" . "</span>";
                    } else if ($status == 1) {
                        echo "<span class='text-danger'>" . "Delivered" . "</span>";;
                    }
                    ?></td>
                <td>
                    <a href="<?php echo site_url('ex_payment_management/money_receipt/' . $e['customer_id']); ?>"
                       class="btn btn-success">View</a>
                    <?php $due = $e['due'];
                    if ($due != 0 && $status == 0) { ?>
                        <a href="<?php echo site_url('ex_payment_management/edit/' . $e['id']); ?>"
                           class="btn btn-info">Edit</a>
                    <?php } ?>
                    <!--                <a href="-->
                    <?php //echo site_url('ex_payment_management/remove/'.$e['id']); ?><!--" class="btn btn-danger">Delete</a>-->
                    <?php $due = $e['due'];
                    if ($due == 0 && $status == 0) { ?>
                        <a href="<?php echo site_url('ex_payment_management/deliver/' . $e['customer_id']); ?>"
                           class="btn btn-danger">Deliver Order</a>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>
<div class="col-md-1"></div>

