<link href="<?php echo base_url() ?>css/payment.css" rel="stylesheet">
<script src="<?php echo base_url() ?>js/jquery-ui.js" xmlns="http://www.w3.org/1999/html"></script>
<link href="<?php echo base_url() ?>css/jquery-ui.css" rel="stylesheet">

<?php $this->load->view('/flashMessage'); ?>
<?php echo form_open('ex_payment_management/add/' . $ex_order_manage['customer_id'], ['id' => 'payment', 'class' => 'form-horizontal']); ?>


<div class="containeree" style=" text-align: center; font-size: 12px">


    <div id="page-wrap1">

        <p id="header1">MONEY RECEIPT</p>


        <div id="identity1">


            <div id="logo1">
                <h2>COMPANY NAME</h2>
                <h4>Address</h4>


            </div>

        </div>

        <!--    </br></br>-->
        <div style="clear:both"></div>

        <div id="customer1">


            <table id="meta1">
                <tr>
                    <td class="meta-head1">Receipt no</td>
                    <td><input type="text" style="text-align: center" name="receipt_no" value="" required/></td>

                </tr>
                <tr>

                    <td class="meta-head1">Date</td>
                    <td><input type="text" style="text-align: center" name="payment_date" id="payment_date" value=""
                               required/></td>
                    <script>
                        $(function () {
                            $("#payment_date").datepicker({
                                dateFormat: "dd-mm-yy",
                            }).datepicker("setDate", "0");
                        });
                    </script>


                </tr>
                <tr>

                    <td class="meta-head1">Delivery Date</td>
                    <td><input type="text" style="text-align: center" name="delivery_date" id="delivery_date" value=""
                               required/></td>
                    <script>
                        $(function () {
                            $("#delivery_date").datepicker({
                                dateFormat: "dd-mm-yy",

                            });
                        });
                    </script>

                </tr>
                <tr>
                    <td class="meta-head1">Customer Name</td>
                    <td>
                        <div class="due1"><?php echo $ex_order_manage['customer_name']; ?></div>
                    </td>
                </tr>
                <tr>
                    <td class="meta-head1">Mobile</td>
                    <td>
                        <div class="due1"><?php echo $ex_order_manage['customer_contact_no']; ?></div>
                    </td>
                </tr>

            </table>

        </div>

        <table id="items1">

            <tr>
                <th style="text-align: center">Sl No</th>
                <th style="text-align: center">Description</th>
                <th style="text-align: center">Quantity</th>

                <th style="text-align: center" colspan="2">Amount</th>
            </tr>
            <?php $sl = 1;
            $t = 0;
            foreach ($ex_order_details as $e) { ?>
                <tr class="item-row">
                    <td class="center1"><?php $sln = $sl++;
                        echo $sln; ?></td>
                    <td class="description1" style="text-align: left">
                        Order No:<?php echo $e['order_no']; ?><br>
                        Item: <?php echo $e['product_name']; ?><br>
                        Measurment:<br> <?php echo $e['customer_measurement']; ?>
                    </td>
                    <td colspan="2" class="center1"><?php echo $e['quantity']; ?></td>
                    <td class="total-value1">
                        <input type="text" style="text-align: center" name="total_amount"
                               value="<?php echo $e['product_regular_price']; ?>"/>
                        <?php $t = $t + (int)$e['product_regular_price']; ?>
                    </td>
                </tr>
            <?php } ?>


            <tr class="hiderow1">
                <td class="hiderow1" colspan="5"></td>
            </tr>

            <tr>
                <td colspan="2" class="blank1"></td>
                <td colspan="2" class="total-line1">Discount</td>
                <td class="total-value1"><input type="text" style="text-align: center" name="discount" id="discount"
                                                value="0"/></td>
            </tr>
            <tr>
                <td colspan="2" class="blank1"></td>
                <td colspan="2" class="total-line1">Total</td>
                <td class="total-value1"><span id="total"><input type="text" style="text-align: center"
                                                                 name="total_amount" id="total_amount"
                                                                 value="<?php echo $t; ?>"/>
                        <?php $t = $t + (int)@$e['product_regular_price']; ?></span></td>
            </tr>


            <tr>
                <td colspan="2" class="blank1"></td>
                <td colspan="2" class="total-line1"> Paid</td>

                <td class="total-value1"><input type="text" style="text-align: center" name="advance" id="advance"
                                                value="" required/></td>
            </tr>
            <tr>
                <td colspan="2" class="blank1"></td>
                <td colspan="2" class="total-line1 balance1"> Due</td>
                <td class="total-value1 balance1">
                    <div class="due1"><input type="text" style="text-align: center" id="due" name="due" value=""/>
                </td>
    </div>
    </td>
    </tr>

    </table>
    <div class="signature" style="margin: 60px 40px">
        <div class="adminsignature" style="float: left;border-top: 1px solid black">Recievers Signature</div>
        <div class="studentsignature" style="float: right;border-top: 1px solid black">Administrator Signature</div>
    </div>
    <div id="terms1">
        <h5 style="margin-top: 100px">Company Name</h5>
        <p>Company Address </p>
    </div>

    <?php echo validation_errors(); ?>
    <button type="submit" class="btn btn-primary btn-block"
            style=" margin: 25px 0px;text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px;">
        PROCEED
    </button>
</div>
</div>
<?php echo form_close(); ?>


<script>
    $(document).ready(function () {


        $("#advance").keyup(function () {
            var discount = $("#discount").val();
            var advance = $("#advance").val();
            var total = $("#total_amount").val();
            var tamount = (total - discount);
            var due = (tamount - advance);
            $("#due").val(due);
        });
    });

</script>

<script>

    $.validator.addMethod("valueNotEquals", function (value, element, arg) {
        return arg != value;
    }, "Value must not equal arg.");


    $("#payment").validate({
        rules: {


            receipt_no: {
                remote: {
                    url: "<?php echo site_url("ex_payment_management/getUniqueReceiptNo");?>",
                    type: "post",
                    dataType: "text",
                    data: {
                        receipt_no: function () {
                            return $('#payment :input[name="receipt_no"]').val();
                        }
                    }

                }
            },

        },
        messages: {


            receipt_no: {
                remote: jQuery.validator.format("{0} is already taken.")

            },


        }


    });

</script>


