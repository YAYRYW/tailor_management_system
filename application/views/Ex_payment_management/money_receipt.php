<link href="<?php echo base_url() ?>css/payment.css" rel="stylesheet">
<script src="<?php echo base_url() ?>js/jquery-ui.js" xmlns="http://www.w3.org/1999/html"></script>
<link href="<?php echo base_url() ?>css/jquery-ui.css" rel="stylesheet">

<?php $this->load->view('/flashMessage'); ?>


<div class="containeree" style=" text-align: center; font-size: 12px">


    <div id="page-wrap1">

        <p id="header1">MONEY RECEIPT</p>


        <div id="identity1">


            <div id="logo1">
                <h2>COMPANY NAME</h2>
                <h4>Address</h4>


            </div>

        </div>

        <!--    </br></br>-->
        <div style="clear:both"></div>

        <div id="customer1">


            <table id="meta1">
                <tr>
                    <td class="meta-head1">Receipt no</td>
                    <td><?php echo $payment_info['receipt_no']; ?></td>

                </tr>
                <tr>

                    <td class="meta-head1">Date</td>
                    <td><?php echo $payment_info['payment_date']; ?></td>


                </tr>
                <tr>

                    <td class="meta-head1">Delivery Date</td>
                    <td><?php echo $payment_info['delivery_date']; ?></td>


                </tr>
                <tr>
                    <td class="meta-head1">Customer Name</td>
                    <td>
                        <div class="due1"><?php echo $ex_order_manage['customer_name']; ?></div>
                    </td>
                </tr>
                <tr>
                    <td class="meta-head1">Mobile</td>
                    <td>
                        <div class="due1"><?php echo $ex_order_manage['customer_contact_no']; ?></div>
                    </td>
                </tr>

            </table>

        </div>

        <table id="items1">

            <tr>
                <th style="text-align: center">Sl No</th>
                <th style="text-align: center">Description</th>
                <th style="text-align: center">Quantity</th>

                <th style="text-align: center" colspan="2">Amount</th>
            </tr>
            <?php $sl = 1;
            $t = 0;
            foreach ($ex_order_details as $e) { ?>
                <tr class="item-row">
                    <td class="center1"><?php $sln = $sl++;
                        echo $sln; ?></td>
                    <td class="description1" style="text-align: left">
                        <b>Order No:</b> <?php echo $e['order_no']; ?><br>
                        <b>Item:</b> <?php echo $e['product_name']; ?><br>
                        <b>Measurement :</b> <br> <?php echo $e['customer_measurement']; ?>
                    </td>
                    <td colspan="2" class="center1"><?php echo $e['quantity']; ?></td>
                    <td class="total-value1">
                        <?php echo $e['product_regular_price']; ?>
                        <?php $t = $t + (int)$e['product_regular_price']; ?>
                    </td>
                </tr>
            <?php } ?>


            <tr class="hiderow1">
                <td class="hiderow1" colspan="5"></td>
            </tr>

            <tr>
                <td colspan="2" class="blank1"></td>
                <td colspan="2" class="total-line1">Discount</td>
                <td class="total-value1"><?php echo $payment_info['discount']; ?></td>
            </tr>
            <tr>
                <td colspan="2" class="blank1"></td>
                <td colspan="2" class="total-line1">Total</td>
                <td class="total-value1"><span id="total"><?php echo $payment_info['total_amount']; ?>
                        <?php $t = $t + (int)@$e['product_regular_price']; ?></span></td>
            </tr>


            <tr>
                <td colspan="2" class="blank1"></td>
                <td colspan="2" class="total-line1"> Paid</td>

                <td class="total-value1"><?php echo $payment_info['advance']; ?></td>
            </tr>
            <tr>
                <td colspan="2" class="blank1"></td>
                <td colspan="2" class="total-line1 balance1"> Due</td>
                <td class="total-value1 balance1">
                    <div class="due1"><?php echo $payment_info['due']; ?></td>
    </div>
    </td>
    </tr>

    </table>
    <div class="signature" style="margin: 60px 40px">
        <div class="adminsignature" style="float: left;border-top: 1px solid black">Recievers Signature</div>
        <div class="studentsignature" style="float: right;border-top: 1px solid black">Administrator Signature</div>
    </div>
    <div id="terms1">
        <h5 style="margin-top: 100px">Company Name</h5>
        <p>Company Address </p>
    </div>

    <div class="pull-right">
        Logged In User: <?php $user = $this->session->userdata('user_name');
        echo $user; ?>
    </div>

</div>
<br>
<button class="btn btn-success">Print</button>
</div>






