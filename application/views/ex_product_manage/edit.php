<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Product Details
            <small>Edit Product Information</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Tables</a></li>
          <li class="active">Data tables</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="register-box">
            <div class="register-box-body">
                <?php $this->load->view('/flashMessage'); ?>
                <?php echo form_open('ex_product_manage/edit/' . $ex_product_manage['product_id'], ['id' => 'edit_product', 'class' => 'form-horizontal']); ?>

                <div class="form-group">
                    <label for="product_name" class=" control-label">Product Name<sup>*</sup></label>
                    <div class="">
                        <input type="text" name="product_name"
                               value="<?php echo($this->input->post('product_name') ? $this->input->post('product_name') : $ex_product_manage['product_name']); ?>"
                               class="form-control" id="product_name" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="product_minimum_price" class=" control-label">Product Minimum Price</label>
                    <div class="">
                        <input type="text" name="product_minimum_price"
                               value="<?php echo($this->input->post('product_minimum_price') ? $this->input->post('product_minimum_price') : $ex_product_manage['product_minimum_price']); ?>"
                               class="form-control" id="product_minimum_price"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="product_regular_price" class=" control-label">Product Regular Price<sup>*</sup></label>
                    <div class="">
                        <input type="text" name="product_regular_price"
                               value="<?php echo($this->input->post('product_regular_price') ? $this->input->post('product_regular_price') : $ex_product_manage['product_regular_price']); ?>"
                               class="form-control" id="product_regular_price" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="discount_%_price" class=" control-label">Discount % Price</label>
                    <div class="">
                        <input type="text" name="discount_%_price"
                               value="<?php echo($this->input->post('discount_%_price') ? $this->input->post('discount_%_price') : $ex_product_manage['discount_%_price']); ?>"
                               class="form-control" id="discount_%_price"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-8">
                        <span class="error"><?php echo validation_errors(); ?></span>
                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
            <!-- /.form-box -->
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    $("#edit_product").validate({});
</script>