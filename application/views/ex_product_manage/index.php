<script>
    $(function () {
        $('#custTable').DataTable({
            "sDom": '<"top`"pf>rt<"bottom"flp><"clear">',
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "autoWidth": true
        });
    });
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php $this->load->view('/flashMessage'); ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Product
            <small>All Products</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Tables</a></li>
          <li class="active">Data tables</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="custTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Product Id</th>
                                <th>Product Name</th>
                                <th>Product Minimum Price</th>
                                <th>Product Regular Price</th>
                                <th>Discount % Price</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($ex_product_manage as $e) { ?>
                                <tr>
                                    <td><?php echo $e['product_id']; ?></td>
                                    <td><?php echo $e['product_name']; ?></td>
                                    <td><?php echo $e['product_minimum_price']; ?></td>
                                    <td><?php echo $e['product_regular_price']; ?></td>
                                    <td><?php echo $e['discount_%_price']; ?></td>
                                    <td>
                                        <div class="btn-group">

                                            <a href="<?php echo site_url('ex_product_manage/edit/' . $e['product_id']); ?>"
                                               style="padding: 1px 4px !important;" class="btn btn-success btn-flat"><i
                                                    class="glyphicon glyphicon-pencil"></i></a>
                                            <div style="float:left;">&nbsp;</div>
                                            <a href="<?php echo site_url('ex_product_manage/remove/' . $e['product_id']); ?>"
                                               style="padding: 1px 4px !important;" class="btn btn-danger btn-flat"><i
                                                    class="glyphicon glyphicon-remove"></i></a>

                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->