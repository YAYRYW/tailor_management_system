<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo site_url("Home_controller/index"); ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>F</b>T</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Freedom</b>Tailors</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="javascript:" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <ul class="dropdown-menu">
                        <!-- inner menu: contains the actual data -->

                    </ul>
                </li>
                <!-- Notifications: style can be found in dropdown.less -->

                <!-- Tasks: style can be found in dropdown.less -->
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo base_url() ?>images/img/user.png" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo $this->session->userdata('full_name'); ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo base_url() ?>images/img/user.png" class="img-circle" alt="User Image">

                            <p><?php echo $this->session->userdata('full_name'); ?></p>
                            <p>
                                <small><?php echo $this->session->userdata('user_contact_no'); ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo base_url() ?>ex_user_manage/change_password"
                                   class="btn btn-default btn-flat">Change Password</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo base_url() ?>login_controller/logout"
                                   class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>