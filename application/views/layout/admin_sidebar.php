<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="javascript:">
                    <i class="fa fa-laptop"></i>
                    <span>Customers</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url("ex_customer_info/index"); ?>"><i class="fa fa-area-chart"></i> All
                            Customers</a></li>
                    <!--                    <li><a href="-->
                    <?php //echo site_url("ex_customer_info/add"); ?><!--"><i class="fa fa-plus-square"></i> Add Customer</a></li>-->
                </ul>
            </li>
            <li class="treeview">
                <a href="javascript:">
                    <i class="fa fa-shopping-cart"></i>
                    <span>Orders</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url("ex_order_manage/index"); ?>"><i class="fa fa-calendar"></i>All
                            Orders</a></li>
                    <li><a href="<?php echo site_url("ex_order_manage/add"); ?>"><i class="fa fa-calendar-plus-o"></i>New
                            Order</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="javascript:">
                    <i class="fa fa-gears"></i> <span>Configuration</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="javscript:"><i class="fa fa-diamond"></i> Product
                            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo site_url("ex_product_manage/index"); ?>"><i
                                        class="fa fa-calendar"></i>All Products</a></li>
                            <li><a href="<?php echo site_url("ex_product_manage/add"); ?>"><i
                                        class="fa fa-calendar-plus-o"></i>New Product</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javscript:"><i class="fa fa-users"></i>User
                            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo site_url("ex_user_manage/index"); ?>"><i class="fa fa-user"></i>All
                                    User</a></li>
                            <li><a href="<?php echo site_url("ex_user_manage/add"); ?>"><i class="fa fa-user-plus"></i>Add
                                    User</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>