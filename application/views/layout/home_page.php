<script src="<?php echo base_url() ?>js/demo.js"></script>
<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php $this->load->view('/flashMessage'); ?>
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner" style="padding-bottom: 0px !important; padding-top: 0px !important;">
                        <table class="ctab">
                            <tr>
                                <td>
                                    <h3><?php $o = $this->Ex_order_manage_model->total_order();
                                        echo $o; ?></h3>
                                </td>
                                <td><p>Total Orders</p></td>
                            </tr>
                            <tr>
                                <td>
                                    <h3><?php $po = $this->Ex_order_manage_model->pending_order();
                                        echo $po; ?></h3>
                                </td>
                                <td>
                                    <p>Not Delivered</p>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="icon">
                        <i class="ion ion ion-bag"></i>
                    </div>
                    <a href="<?php echo site_url("ex_order_manage/index"); ?>" class="small-box-footer">See All Orders
                        <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?php $t = $this->Ex_customer_info_model->total_customer();
                            echo $t; ?></h3>
                        <p>Customers</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="<?php echo site_url("ex_customer_info/index"); ?>" class="small-box-footer">More info <i
                            class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- /.content-wrapper -->


<script src="<?php echo base_url() ?>js/pages/dashboard.js"></script>