<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS
       ================================================== -->


    <!-- Bootstrap css file-->
    <link href="<?php echo base_url() ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font awesome css file-->
    <!--    <link href="--><?php //echo base_url() ?><!--styles/font-awesome.min.css" rel="stylesheet">-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <!--    <link href="--><?php //echo base_url() ?><!--styles/ionicons.min.css" rel="stylesheet">-->
    <link href="<?php echo base_url() ?>styles/AdminLTE.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>styles/skins/_all-skins.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>plugins/iCheck/square/blue.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>plugins/datepicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>plugins/daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet">


    <script src="<?php echo base_url() ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="<?php echo base_url() ?>plugins/iCheck/icheck.min.js"></script>
<!--    <script>-->
<!--        $(function () {-->
<!--            $('input').iCheck({-->
<!--                checkboxClass: 'icheckbox_square-blue',-->
<!--                radioClass: 'iradio_square-blue',-->
<!--                increaseArea: '20%' // optional-->
<!--            });-->
<!--        });-->
<!--    </script>-->
    <script src="<?php echo base_url() ?>bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/morris/morris.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?php echo base_url() ?>plugins/knob/jquery.knob.js"></script>
    <script src="<?php echo base_url() ?>js/moment.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/jQuery/jquery-2.2.3.min.js"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url() ?>plugins/select2/select2.full.min.js"></script>
    <!-- chosen -->
    <script src="<?php echo base_url() ?>plugins/chosen/chosen.jquery.js"></script>
    <!-- JqueryUI -->
    <script src="<?php echo base_url() ?>plugins/jQueryUI/jquery-ui-1.11.4.js"></script>
    <link rel="stylesheet" href="<?php echo base_url() ?>plugins/jQueryUI/jquery-ui.css">
    <!-- InputMask -->
    <script src="<?php echo base_url() ?>plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?php echo base_url() ?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="<?php echo base_url() ?>plugins/input-mask/jquery.inputmask.extensions.js"></script>


    <!-- bootstrap color picker -->
    <script src="<?php echo base_url() ?>plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="<?php echo base_url() ?>plugins/timepicker/bootstrap-timepicker.min.js"></script>

    <!-- datetimepicker -->
    <script src="<?php echo base_url() ?>plugins/datetimepicker/jquery.datetimepicker.full.js"></script>


    <script src="<?php echo base_url() ?>plugins/daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url() ?>plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url() ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/fastclick/fastclick.js"></script>
    <script src="<?php echo base_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>js/app.min.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <!-- Morris.js charts -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script> -->


    <style>
        .error {
            color: red;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }
    </style>

    <script>
        $(function () {
            $('#userTable').DataTable({
                "sDom": '<"top`"pf>rt<"bottom"flp><"clear">',
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        });
    </script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">