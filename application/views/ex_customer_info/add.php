<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customer Details
            <small>Add Customer Information</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Tables</a></li>
          <li class="active">Data tables</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="register-box">
            <div class="register-box-body">
                <?php echo form_open('ex_customer_info/add', ['id' => 'add_customer']); ?>
                <div class="form-group has-feedback">
                    <label>Customer Name</label>
                    <input type="text" name="customer_name" value="<?php echo $this->input->post('customer_name'); ?>"
                           class="form-control" id="customer_name" required/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <label>Address</label>
                    <!--						<input type="text" class="form-control" placeholder="Customer Address">-->
                    <textarea name="customer_address" class="form-control" id="customer_address"
                              required><?php echo $this->input->post('customer_address'); ?></textarea>
                    <span class="glyphicon glyphicon-home form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <label>Contact No</label>
                    <input type="text" name="customer_contact_no"
                           value="<?php echo $this->input->post('customer_contact_no'); ?>" class="form-control"
                           id="customer_contact_no" required/>
                    <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <span class="error"><?php echo validation_errors(); ?></span>
                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Create</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.form-box -->
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    $("#add_customer").validate({});
</script>