<script>
    $(function () {
        $('#custTable').DataTable({
            "sDom": '<"top`"pf>rt<"bottom"flp><"clear">',
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": false,
            "info": false,
            "autoWidth": true
        });
    });
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php $this->load->view('/flashMessage'); ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Customers
            <small>All Customers</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Tables</a></li>
          <li class="active">Data tables</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="custTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Contact</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($ex_customer_info as $e) { ?>
                                <tr>
                                    <td><?php echo $e['customer_id']; ?></td>
                                    <td><?php echo $e['customer_name']; ?></td>
                                    <td><?php echo $e['customer_address']; ?></td>
                                    <td><?php echo $e['customer_contact_no']; ?></td>
                                    <td>
                                        <div class="btn-group">

                                            <a href="<?php echo site_url('ex_customer_info/edit/' . $e['customer_id']); ?>"
                                               style="padding: 1px 4px !important;" class="btn btn-success btn-flat"><i
                                                    class="glyphicon glyphicon-pencil"></i></a>
                                            <div style="float:left;">&nbsp;</div>
                                            <!--										<a href="-->
                                            <?php //echo site_url('ex_customer_info/remove/'.$e['customer_id']); ?><!--"  style="padding: 1px 4px !important;" class="btn btn-danger btn-flat"><i class="glyphicon glyphicon-remove"></i></a>-->

                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->









