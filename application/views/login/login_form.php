<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Freedom Tailors | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS
       ================================================== -->
    <script src="<?php echo base_url() ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?php echo base_url() ?>bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/iCheck/icheck.min.js"></script>
    <!-- Bootstrap css file-->
    <link href="<?php echo base_url() ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font awesome css file-->
    <!--    <link href="--><?php //echo base_url() ?><!--css/font-awesome.min.css" rel="stylesheet">-->
    <link href="<?php echo base_url() ?>styles/AdminLTE.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>plugins/iCheck/square/blue.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>

</head>
<body class="hold-transition login-page">


<?php $this->load->view('/flashMessage'); ?>
<div class="login-box">
    <div class="login-logo">
        <a href="javascript:"><b>Freedom</b>Tailors</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <?php echo form_open_multipart('login_controller/get_login', ['name' => 'form-login']); ?>
        <!--            <form action="javascript:" method="post">-->
        <div class="form-group has-feedback">
            <label>Username</label>
            <input type="text" class="form-control" placeholder="User Name" id="user" name="user_name">

        </div>
        <div class="form-group has-feedback">
            <label>Password</label>
            <input type="password" class="form-control" id="pass" placeholder="Password" name="user_password">

        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label>
                        <input type="checkbox"> Remember Me
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">

                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div>
            <!-- /.col -->
        </div>
        <?php echo form_close(); ?>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


</body>
</html>
