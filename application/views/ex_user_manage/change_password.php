<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Change Password
            <small>Change Password Information</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Tables</a></li>
          <li class="active">Data tables</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="register-box">
            <div class="register-box-body">
                <?php $this->load->view('/flashMessage'); ?>
                <?php echo form_open('ex_user_manage/change_password', ['id' => 'pass']); ?>

                <div class="form-group has-feedback">
                    <label>Change Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Current Password" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="New Password" name="new_password"
                           id="new_password" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Retype password" name="password_confirm"
                           data-rule-equalTo="#new_password" required>
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <span class="error"><?php echo validation_errors(); ?></span>
                        <button type="button" class="btn btn-success bg-red"
                                onClick="window.location='<?php echo site_url("Home_controller/index"); ?>'"/>
                        Cancel</button>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Change</button>
                    </div>

                    <?php echo form_close(); ?>
                </div>
                <!-- /.form-box -->
            </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>

    jQuery('#pass').validate({
        rules: {
            password: {
                minlength: 6
            },
            password_confirm: {
                minlength: 6,
                equalTo: '[name="new_password"]'
            }
        }
    });
</script>
