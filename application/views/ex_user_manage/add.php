<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Register
            <small>Register a new user</small>
        </h1>
        <?php $this->load->view('/flashMessage'); ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="register-box">
            <div class="register-box-body">
                <?php echo form_open('ex_user_manage/add', ['id' => 'add_user', 'class' => '']); ?>
                <div class="form-group has-feedback">
                    <label>Fullname</label>
                    <input type="text" class="form-control" placeholder="Full name" name="full_name"
                           value="<?php echo $this->input->post('full_name'); ?>" class="form-control" id="full_name"
                           required/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <label>Address</label>
                    <input type="text" class="form-control" placeholder=" Address" name="user_address"
                           value="<?php echo $this->input->post('user_address'); ?>" id="user_address" required/>
                    <span class="glyphicon glyphicon-home form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <label>Contact No</label>
                    <input type="text" placeholder="Contact no" name="user_contact_no"
                           value="<?php echo $this->input->post('user_contact_no'); ?>" class="form-control"
                           id="user_contact_no" required/>
                    <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <label>Username</label>
                    <input type="text" placeholder="User name" name="user_name"
                           value="<?php echo $this->input->post('user_name'); ?>" class="form-control" id="user_name"
                           required/>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group">
                    <label>Role</label>
                    <select name="user_role_id" class="form-control" required>
                        <option value="">select</option>
                        <?php
                        foreach ($all_ex_role as $ex_role) {
                            $selected = ($ex_role['role_id'] == $this->input->post('user_role_id')) ? ' selected="selected"' : null;

                            echo '<option value="' . $ex_role['role_id'] . '" ' . $selected . '>' . $ex_role['role_type'] . '</option>';
                        }
                        ?>
                    </select>
                </div>

                <div class="form-group has-feedback">
                    <label>Password</label>
                    <input type="password" placeholder="Password" name="user_password"
                           value="<?php echo $this->input->post('user_password'); ?>" class="form-control"
                           id="user_password" required/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Retype password" name="password_confirm"
                           data-rule-equalTo="#new_password" required>
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">

                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <span class="error"><?php echo validation_errors(); ?></span>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                    </div>
                    <!-- /.col -->
                </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.form-box -->
        </div>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    $("#add_user").validate({
        rules: {
            user_name: {
                remote: {
                    url: "<?php echo site_url("ex_user_manage/getUniqueuser");?>",
                    type: "post",
                    dataType: "text",
                    data: {
                        user_name: function () {
                            return $('#add_user :input[name="user_name"]').val();
                        }
                    }

                }
            },
            user_password: {
                minlength: 6
            },
            password_confirm: {
                minlength: 6,
                equalTo: '[name="new_password"]'
            }
        },
        messages: {


            user_name: {
                remote: jQuery.validator.format("{0} is already taken.")

            },


        }


    });
</script>

