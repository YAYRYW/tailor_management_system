<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User
            <small>All Users</small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Tables</a></li>
          <li class="active">Data tables</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <?php $this->load->view('/flashMessage'); ?>
                        <table id="userTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Full Name</th>
                                <th>Address</th>
                                <th>Contact</th>
                                <th>User Name</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach ($ex_user_manage as $e) { ?>
                                <tr>
                                    <td><?php echo $e['user_id']; ?></td>
                                    <td><?php echo $e['full_name']; ?></td>
                                    <td><?php echo $e['user_address']; ?></td>
                                    <td><?php echo $e['user_contact_no']; ?></td>
                                    <td><?php echo $e['user_name']; ?></td>
                                    <td><?php echo $e['role_type']; ?></td>
                                    <td>
                                        <div class="btn-group">

                                            <a href="<?php echo site_url('ex_user_manage/edit/' . $e['user_id']); ?>"
                                               style="padding: 1px 4px !important;" class="btn btn-success btn-flat"><i
                                                    class="glyphicon glyphicon-pencil"></i></a>
                                            <div style="float:left;">&nbsp;</div>
                                            <a href="<?php echo site_url('ex_user_manage/remove/' . $e['user_id']); ?>"
                                               style="padding: 1px 4px !important;" class="btn btn-danger btn-flat"><i
                                                    class="glyphicon glyphicon-remove"></i></a>

                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


