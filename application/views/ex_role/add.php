<div class="col-md-1"></div>
<div class="col-md-8">
    <?php $this->load->view('/flashMessage'); ?>
    <?php echo form_open('ex_role/add', ['id' => 'add_role', 'class' => 'form-horizontal']); ?>


    <div class="form-group">
        <label for="role_type" class="col-md-4 control-label">Role <sup>*</sup></label>
        <div class="col-md-4">
            <input type="text" name="role_type" value="<?php echo $this->input->post('role_type'); ?>"
                   class="form-control" id="role_type" required/>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-8">
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </div>

    <?php echo form_close(); ?>
</div>
<div class="col-md-3"></div>

<script>
    $("#add_role").validate({});
</script>
