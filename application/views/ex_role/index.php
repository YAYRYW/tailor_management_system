<div class="col-md-4"></div>
<div class="col-md-5">
    <?php $this->load->view('/flashMessage'); ?>
    <div class="pull-right">
        <a href="<?php echo site_url('ex_role/add'); ?>" style="margin-bottom: 20px" class="btn btn-success">Add</a>
    </div>

    <table class="table table-bordered table-striped table-hover table-condensed table-responsive text-center">
        <tr>
            <th class="text-center">Role Id</th>
            <th class="text-center">Role Type</th>
            <th class="text-center">Actions</th>
        </tr>
        <?php foreach ($ex_role as $e) { ?>
            <tr>
                <td><?php echo $e['role_id']; ?></td>
                <td><?php echo $e['role_type']; ?></td>
                <td>
                    <a href="<?php echo site_url('ex_role/edit/' . $e['role_id']); ?>" class="btn btn-info">Edit</a>
                    <a href="<?php echo site_url('ex_role/remove/' . $e['role_id']); ?>"
                       class="btn btn-danger">Delete</a>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>
<div class="col-md-3"></div>