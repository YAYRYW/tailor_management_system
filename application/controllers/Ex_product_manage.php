<?php


class Ex_product_manage extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('log_in')) {
            return redirect('login_controller');
        }
        $this->load->model('Ex_product_manage_model');
    }

    /*
     * Listing of ex_product_manage
     */
    function index()
    {
        $data['ex_product_manage'] = $this->Ex_product_manage_model->get_all_ex_product_manage();
        $data['title'] = "View Product";
        $data['content'] = 'ex_product_manage/index';
        $this->load->vars($data);
        $this->load->view('layout/main_layout');
    }

    /*
     * Adding a new ex_product_manage
     */
    function add()
    {
        if (isset($_POST) && count($_POST) > 0) {
            $params = array(
                'product_name' => $this->input->post('product_name'),
                'product_minimum_price' => $this->input->post('product_minimum_price'),
                'product_regular_price' => $this->input->post('product_regular_price'),
                'discount_%_price' => $this->input->post('discount_%_price'),
            );

            $ex_product_manage_id = $this->Ex_product_manage_model->add_ex_product_manage($params);
            $this->session->set_flashdata('message', "Successfully Saved");
            redirect('ex_product_manage/index');
        } else {
            $data['title'] = "Add Product";
            $data['content'] = 'ex_product_manage/add';
            $this->load->vars($data);
            $this->load->view('layout/main_layout');
        }
    }

    /*
     * Editing a ex_product_manage
     */
    function edit($product_id)
    {
        // check if the ex_product_manage exists before trying to edit it
        $ex_product_manage = $this->Ex_product_manage_model->get_ex_product_manage($product_id);

        if (isset($ex_product_manage['product_id'])) {
            if (isset($_POST) && count($_POST) > 0) {
                $params = array(
                    'product_name' => $this->input->post('product_name'),
                    'product_minimum_price' => $this->input->post('product_minimum_price'),
                    'product_regular_price' => $this->input->post('product_regular_price'),
                    'discount_%_price' => $this->input->post('discount_%_price'),
                );

                $this->Ex_product_manage_model->update_ex_product_manage($product_id, $params);
                $this->session->set_flashdata('message', "Successfully updated");
                redirect('ex_product_manage/index');
            } else {
                $data['ex_product_manage'] = $this->Ex_product_manage_model->get_ex_product_manage($product_id);
                $data['title'] = "Edit Product";
                $data['content'] = 'ex_product_manage/edit';
                $this->load->vars($data);
                $this->load->view('layout/main_layout');
            }
        } else
            show_error('The ex_product_manage you are trying to edit does not exist.');
    }

    /*
     * Deleting ex_product_manage
     */
    function remove($product_id)
    {
        $ex_product_manage = $this->Ex_product_manage_model->get_ex_product_manage($product_id);

        // check if the ex_product_manage exists before trying to delete it
        if (isset($ex_product_manage['product_id'])) {
            $this->Ex_product_manage_model->delete_ex_product_manage($product_id);
            $this->session->set_flashdata('error', "Successfully Deleted");
            redirect('ex_product_manage/index');
        } else
            show_error('The ex_product_manage you are trying to delete does not exist.');
    }

    function get_product_data_by_product_id()
    {
        $productID = $_POST['product_id'];// $this->input->post('product_id');

        $productData = $this->Ex_product_manage_model->get_ex_product_price($productID);
        //print_r($productData);
        //echo json_encode($productData);
        echo $productData['product_regular_price'];
    }
}
