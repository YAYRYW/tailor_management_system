<?php


class Ex_user_manage extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('log_in')) {
            return redirect('login_controller');
        }
        $this->load->model('Ex_user_manage_model');
    }

    /*
     * Listing of ex_user_manage
     */
    function index()
    {
        $data['ex_user_manage'] = $this->Ex_user_manage_model->get_all_ex_user_manage();
//print_r($this->Ex_user_manage_model->get_all_ex_user_manage());die();
        $data['title'] = "User List";
        $data['content'] = 'ex_user_manage/index';
        $this->load->vars($data);
        $this->load->view('layout/main_layout');
    }

    /*
     * Adding a new ex_user_manage
     */
    function add()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('user_password', 'User Password', 'required|min_length[6]');
        $this->form_validation->set_rules('user_name', 'User Name', 'required');
        $this->form_validation->set_rules('full_name', 'Full Name', 'required');
        $this->form_validation->set_rules('user_contact_no', 'Contact No', 'required');
        $this->form_validation->set_rules('user_address', 'Address', 'required');


        if ($this->form_validation->run()) {
            $password = sha1($this->input->post('user_password'));
            $params = array(
                'user_name' => $this->input->post('user_name'),
                'full_name' => $this->input->post('full_name'),
                'user_address' => $this->input->post('user_address'),
                'user_contact_no' => $this->input->post('user_contact_no'),
                'user_role_id' => $this->input->post('user_role_id'),
                'user_password' => $password,
            );

            $ex_user_manage_id = $this->Ex_user_manage_model->add_ex_user_manage($params);
            $this->session->set_flashdata('message', "Successfully Saved");
            redirect('ex_user_manage/index');
        } else {

            $this->load->model('Ex_role_model');
            $data['all_ex_role'] = $this->Ex_role_model->get_all_ex_role();
            $data['title'] = "User Add";
            $data['content'] = 'ex_user_manage/add';
            $this->load->vars($data);
            $this->load->view('layout/main_layout');
        }
    }

    /*
     * Editing a ex_user_manage
     */
    function edit($user_id)
    {
        // check if the ex_user_manage exists before trying to edit it
        $ex_user_manage = $this->Ex_user_manage_model->get_ex_user_manage($user_id);

        if (isset($ex_user_manage['user_id'])) {
            $this->load->library('form_validation');

//            $this->form_validation->set_rules('user_password','User Password','required|min_length[6]');
            $this->form_validation->set_rules('user_name', 'User Name', 'required');
            $this->form_validation->set_rules('full_name', 'Full Name', 'required');
            $this->form_validation->set_rules('user_contact_no', 'Contact No', 'required');
            $this->form_validation->set_rules('user_address', 'Address', 'required');

            if ($this->form_validation->run()) {
                $params = array(
                    'user_name' => $this->input->post('user_name'),
                    'full_name' => $this->input->post('full_name'),
                    'user_address' => $this->input->post('user_address'),
                    'user_contact_no' => $this->input->post('user_contact_no'),
                    'user_role_id' => $this->input->post('user_role_id'),

                );

                $this->Ex_user_manage_model->update_ex_user_manage($user_id, $params);
                $this->session->set_flashdata('message', "Successfully Updated");
                redirect('ex_user_manage/index');
            } else {
                $data['ex_user_manage'] = $this->Ex_user_manage_model->get_ex_user_manage($user_id);

                $this->load->model('Ex_role_model');
                $data['all_ex_role'] = $this->Ex_role_model->get_all_ex_role();
                $data['title'] = "User Edit";
                $data['content'] = 'ex_user_manage/edit';
                $this->load->vars($data);
                $this->load->view('layout/main_layout');
            }
        } else
            show_error('The ex_user_manage you are trying to edit does not exist.');
    }

    /*
     * Deleting ex_user_manage
     */
    function remove($user_id)
    {
        $ex_user_manage = $this->Ex_user_manage_model->get_ex_user_manage($user_id);

        // check if the ex_user_manage exists before trying to delete it
        if (isset($ex_user_manage['user_id'])) {
            $this->Ex_user_manage_model->delete_ex_user_manage($user_id);
            $this->session->set_flashdata('error', "Successfully Deleted");
            redirect('ex_user_manage/index');
        } else
            show_error('The ex_user_manage you are trying to delete does not exist.');
    }

    function getUniqueuser()
    {
        $user_name = $_POST['user_name'];
        $uniqueuser_name = $this->Ex_user_manage_model->checkUser($user_name);

        if ($uniqueuser_name > 0) {
            echo 'Already Exists please choose another';
        } else {
            echo 'true';
        }
    }

    function change_password()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'password', 'required|trim');
        $this->form_validation->set_rules('new_password', 'new_Password', 'required');


        if (($this->form_validation->run())) {


            $new = sha1($this->input->post('new_password'));
            $password = sha1($this->input->post('password'));
            $user_id = $this->session->userdata('user_id');
            $userData = $this->Ex_user_manage_model->pass_check($password);
            if ($userData <> 0) {
                $params = array(
                    'user_password' => $new,
                );

                $this->Ex_user_manage_model->update_ex_user_manage($user_id, $params);
                $this->session->set_flashdata('message', "Sucessfully Updated");
                return redirect('home_controller/index');
            } else {
                $this->session->set_flashdata('error', "Error: Not Updated");
                return redirect('login_controller/index');
            }

        } else {

            $data['title'] = "Change Password";
            $data['content'] = 'ex_user_manage/change_password';
            $this->load->vars($data);
            $this->load->view('layout/main_layout');
        }
    }




}
