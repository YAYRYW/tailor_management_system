<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_controller extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('log_in')) {
            return redirect('login_controller');
        }
        $this->load->model('Ex_order_manage_model');
        $this->load->model('Ex_customer_info_model');
    }


    public function index()
    {

        $data['title'] = "HOME";
        $this->load->view('layout/home_layout', $data);
    }


}
