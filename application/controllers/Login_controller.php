<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();


    }

    /*
     * Listing of student_registration
     */
    function index()
    {
        $status=$this->session->userdata('log_in');
        if ($status==1) {
            return redirect('home_controller/index');
        }
        $this->load->view('login/login_form');

    }

    public function get_login()
    {


        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', 'User Name', 'required|trim');

        $this->form_validation->set_rules('user_password', 'Password', 'required');


        if (!($this->form_validation->run())) {

            redirect('login_controller/index');

        } else {

            $user_name = $this->input->post('user_name');
            $password = sha1($this->input->post('user_password'));
            $this->load->model('Ex_user_manage_model');

            $userData = $this->Ex_user_manage_model->login_check($user_name, $password);
//            print_r($password);die();


            // $sess_array = array();
            foreach ($userData as $row) {


                $this->session->set_userdata('user_id', $row->user_id);
                $this->session->set_userdata('user_name', $row->user_name);
                $this->session->set_userdata('full_name', $row->full_name);
                $this->session->set_userdata('user_address', $row->user_address);
                $this->session->set_userdata('user_contact_no', $row->user_contact_no);
                $this->session->set_userdata('user_role_id', $row->user_role_id);
                $this->session->set_userdata('log_in', true);
            }


            if ($userData <> 0) {
                $this->session->set_flashdata('message', "Wellcome");
                return redirect('home_controller/index');
            } else {
                $this->session->set_flashdata('error', "Invalid Username or Password");
                return redirect('login_controller/index');
            }
        }
    }

    public function logout()
    {
//        $employee_id = $this->session->userdata('employee_id');
//        $this->User_model->insert_logout($employee_id);
        $this->session->unset_userdata('log_in');
        $this->session->sess_destroy();

        return redirect('login_controller/index');
    }

}
