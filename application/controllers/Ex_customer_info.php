<?php


class Ex_customer_info extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('log_in')) {
            return redirect('login_controller');
        }
        $this->load->model('Ex_customer_info_model');
    }

    /*
     * Listing of ex_customer_info
     */
    function index()
    {
        $data['ex_customer_info'] = $this->Ex_customer_info_model->get_all_ex_customer_info();
        $data['title'] = "Customer List";
        $data['content'] = 'ex_customer_info/index';
        $this->load->vars($data);
        $this->load->view('layout/main_layout');
    }

    /*
     * Adding a new ex_customer_info
     */
    function add()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('customer_name', 'Customer Name', 'required|max_length[100]');
        $this->form_validation->set_rules('customer_contact_no', 'Customer Contact No', 'required|max_length[100]');

        if ($this->form_validation->run()) {
            $params = array(
                'customer_name' => $this->input->post('customer_name'),
                'customer_address' => $this->input->post('customer_address'),
                'customer_contact_no' => $this->input->post('customer_contact_no'),
            );

            $ex_customer_info_id = $this->Ex_customer_info_model->add_ex_customer_info($params);
            $this->session->set_flashdata('message', "Successfully Saved");
            redirect('ex_customer_info/index');
        } else {
            $data['title'] = "Customer Add";
            $data['content'] = 'ex_customer_info/add';
            $this->load->vars($data);
            $this->load->view('layout/main_layout');
        }
    }

    /*
     * Editing a ex_customer_info
     */
    function edit($customer_id)
    {
        // check if the ex_customer_info exists before trying to edit it
        $ex_customer_info = $this->Ex_customer_info_model->get_ex_customer_info($customer_id);

        if (isset($ex_customer_info['customer_id'])) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('customer_name', 'Customer Name', 'required|max_length[100]');
            $this->form_validation->set_rules('customer_contact_no', 'Customer Contact No', 'required|max_length[100]');

            if ($this->form_validation->run()) {
                $params = array(
                    'customer_name' => $this->input->post('customer_name'),
                    'customer_address' => $this->input->post('customer_address'),
                    'customer_contact_no' => $this->input->post('customer_contact_no'),
                );

                $this->Ex_customer_info_model->update_ex_customer_info($customer_id, $params);
                $this->session->set_flashdata('message', "Successfully Updated");
                redirect('ex_customer_info/index');
            } else {
                $data['ex_customer_info'] = $this->Ex_customer_info_model->get_ex_customer_info($customer_id);
                $data['title'] = "Customer Edit";
                $data['content'] = 'ex_customer_info/edit';
                $this->load->vars($data);
                $this->load->view('layout/main_layout');
            }
        } else
            show_error('The ex_customer_info you are trying to edit does not exist.');
    }

    /*
     * Deleting ex_customer_info
     */
    function remove($customer_id)
    {
        $ex_customer_info = $this->Ex_customer_info_model->get_ex_customer_info($customer_id);

        // check if the ex_customer_info exists before trying to delete it
        if (isset($ex_customer_info['customer_id'])) {
            $this->Ex_customer_info_model->delete_ex_customer_info($customer_id);
            $this->session->set_flashdata('error', "Successfully Deleted");
            redirect('ex_customer_info/index');
        } else
            show_error('The ex_customer_info you are trying to delete does not exist.');
    }

    function get_customer_data()
    {
        $customer_contact_no = $_POST['customer_contact_no'];// $this->input->post('memberShipID');
        $userData = $this->Ex_customer_info_model->customer_data($customer_contact_no);
        print_r(json_encode($userData));
    }

}
