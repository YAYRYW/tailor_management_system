<?php


class Ex_role extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('log_in')) {
            return redirect('login_controller');
        }
        $this->load->model('Ex_role_model');
    }

    /*
     * Listing of ex_role
     */
    function index()
    {
        $data['ex_role'] = $this->Ex_role_model->get_all_ex_role();


        $data['content'] = 'ex_role/index';
        $this->load->vars($data);
        $this->load->view('layout/main_layout');
    }

    /*
     * Adding a new ex_role
     */
    function add()
    {
        if (isset($_POST) && count($_POST) > 0) {
            $params = array(

                'role_type' => $this->input->post('role_type'),
            );

            $ex_role_id = $this->Ex_role_model->add_ex_role($params);
            $this->session->set_flashdata('message', "Successfully Saved");
            redirect('ex_role/index');
        } else {
            $data['content'] = 'ex_role/add';
            $this->load->vars($data);
            $this->load->view('layout/main_layout');
        }
    }

    /*
     * Editing a ex_role
     */
    function edit($role_id)
    {
        // check if the ex_role exists before trying to edit it
        $ex_role = $this->Ex_role_model->get_ex_role($role_id);

        if (isset($ex_role['role_id'])) {
            if (isset($_POST) && count($_POST) > 0) {
                $params = array(

                    'role_type' => $this->input->post('role_type'),
                );

                $this->Ex_role_model->update_ex_role($role_id, $params);
                $this->session->set_flashdata('message', "Successfully Updated");
                redirect('ex_role/index');
            } else {
                $data['ex_role'] = $this->Ex_role_model->get_ex_role($role_id);

                $data['content'] = 'ex_role/edit';
                $this->load->vars($data);
                $this->load->view('layout/main_layout');
            }
        } else
            show_error('The ex_role you are trying to edit does not exist.');
    }

    /*
     * Deleting ex_role
     */
    function remove($role_id)
    {
        $ex_role = $this->Ex_role_model->get_ex_role($role_id);

        // check if the ex_role exists before trying to delete it
        if (isset($ex_role['role_id'])) {
            $this->Ex_role_model->delete_ex_role($role_id);
            $this->session->set_flashdata('message', "Successfully Removed");
            redirect('ex_role/index');
        } else
            show_error('The ex_role you are trying to delete does not exist.');
    }

}
