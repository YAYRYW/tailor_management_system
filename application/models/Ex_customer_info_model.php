<?php


class Ex_customer_info_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get ex_customer_info by customer_id
     */
    function get_ex_customer_info($customer_id)
    {
        return $this->db->get_where('ex_customer_info', array('customer_id' => $customer_id))->row_array();
    }

    /*
     * Get all ex_customer_info
     */
    function get_all_ex_customer_info()
    {
        $this->db->select('*');
        $this->db->from('ex_customer_info');
        $this->db->order_by('customer_id', 'desc');
        $query = $this->db->get()->result_array();
        return $query;
    }

    /*
     * function to add new ex_customer_info
     */
    function add_ex_customer_info($params)
    {
        $this->db->insert('ex_customer_info', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update ex_customer_info
     */
    function update_ex_customer_info($customer_id, $params)
    {
        $this->db->where('customer_id', $customer_id);
        $response = $this->db->update('ex_customer_info', $params);
        if ($response) {
            return "ex_customer_info updated successfully";
        } else {
            return "Error occuring while updating ex_customer_info";
        }
    }

    /*
     * function to delete ex_customer_info
     */
    function delete_ex_customer_info($customer_id)
    {
        $response = $this->db->delete('ex_customer_info', array('customer_id' => $customer_id));
        if ($response) {
            return "ex_customer_info deleted successfully";
        } else {
            return "Error occuring while deleting ex_customer_info";
        }
    }

    function total_customer()
    {
        $this->db->select('*');
        $this->db->from('ex_customer_info');
        $this->db->order_by('customer_id', 'desc');
        $query = $this->db->get()->num_rows();
        return $query;
    }

    function customer_data($customer_contact_no)
    {
        $query = $this->db->query(
            "SELECT *            
             FROM ex_customer_info
             WHERE customer_contact_no Like '$customer_contact_no%'  
             
            "
        );

        return $query->result_array();
    }
    function customer_data_by_contact_no($customer_contact_no)
    {
        $query = $this->db->query(
            "SELECT *            
             FROM ex_customer_info
             WHERE customer_contact_no =$customer_contact_no  
             
            "
        );

        return @$query->result_array()[0];
    }

}
