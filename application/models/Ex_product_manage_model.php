<?php


class Ex_product_manage_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get ex_product_manage by product_id
     */
    function get_ex_product_manage($product_id)
    {
        return $this->db->get_where('ex_product_manage', array('product_id' => $product_id))->row_array();
    }

    function get_ex_product_price($product_id)
    {
        $query = $this->db->query("
                                  SELECT product_regular_price 
                                  FROM ex_product_manage
                                  WHERE  product_id = $product_id ");

        return $query->result_array()[0];
    }

    /*
     * Get all ex_product_manage
     */
    function get_all_ex_product_manage()
    {
        return $this->db->get('ex_product_manage')->result_array();
    }

    /*
     * function to add new ex_product_manage
     */
    function add_ex_product_manage($params)
    {
        $this->db->insert('ex_product_manage', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update ex_product_manage
     */
    function update_ex_product_manage($product_id, $params)
    {
        $this->db->where('product_id', $product_id);
        $response = $this->db->update('ex_product_manage', $params);
        if ($response) {
            return "ex_product_manage updated successfully";
        } else {
            return "Error occuring while updating ex_product_manage";
        }
    }

    /*
     * function to delete ex_product_manage
     */
    function delete_ex_product_manage($product_id)
    {
        $response = $this->db->delete('ex_product_manage', array('product_id' => $product_id));
        if ($response) {
            return "ex_product_manage deleted successfully";
        } else {
            return "Error occuring while deleting ex_product_manage";
        }
    }
}
