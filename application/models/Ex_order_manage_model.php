<?php

/*
 * Generated by CRUDigniter v3.0 Beta 
 * www.crudigniter.com
 */

class Ex_order_manage_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get ex_order_manage by id
     */
    function get_ex_order_manage($id)
    {
        return $this->db->get_where('ex_order_manage', array('id' => $id))->row_array();
    }

    function get_ex_order_manage_order_no($order_no)
    {
        return $this->db->get_where('ex_order_manage', array('order_no' => $order_no))->result_array();
    }

    /*
     * Get all ex_order_manage
     */
    function get_all_ex_order_manage()
    {
        return $this->db->get('ex_order_manage')->result_array();
    }

    /*
     * function to add new ex_order_manage
     */
    function add_ex_order_manage($params)
    {
        $this->db->insert('ex_order_manage', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update ex_order_manage
     */
    function update_ex_order_manage($id, $params)
    {
        $this->db->where('order_no', $id);
        $response = $this->db->update('ex_order_manage', $params);
        if ($response) {
            return "ex_order_manage updated successfully";
        } else {
            return "Error occuring while updating ex_order_manage";
        }
    }

    /*
     * function to delete ex_order_manage
     */
    function delete_ex_order_manage($id)
    {
        $response = $this->db->delete('ex_order_manage', array('id' => $id));
        if ($response) {
            return "ex_order_manage deleted successfully";
        } else {
            return "Error occuring while deleting ex_order_manage";
        }
    }

    function checkOrder_no($order_no)
    {
        $Info = $this->db->query("SELECT `order_no` FROM `ex_order_manage`
        WHERE `order_no` = $order_no LIMIT 1");


        return $Info->num_rows();
    }

    function get_order_number()
    {
        $query = $this->db->query("SELECT MAX(CAST(order_no AS int)) AS order_no FROM ex_order_manage");


        return @$query->result_array()[0];
    }

    function view_order_list()
    {
        $query = $this->db->query("SELECT om.order_no,
                 om.order_date,
                 om.customer_id,
                 om.delivery_date,
                 om.total_pay,
                 om.discount,
                 om.paid,
                 om.due,
                 om.order_status,
                 om.user_id,
                 
                 c.*
             FROM ex_order_manage om
           
             
             JOIN ex_customer_info c ON om.customer_id=c.customer_id ");


        return @$query->result_array();
    }

    function view_product_by_order_id($order_no)
    {
        $query = $this->db->query("SELECT p.*
FROM order_details od 
JOIN ex_product_manage p 
ON od.product_id=p.product_id 
WHERE od.order_number=$order_no ");


        return @$query->result_array();
    }

    function view_indevidual_order_list_by_order_no($order_no)
    {
        $query = $this->db->query("SELECT om.*,od.*,c.*,p.*,u.*,pm.*
FROM `ex_order_manage` om 
JOIN order_details od ON om.order_no = od.order_number
JOIN ex_customer_info c ON c.customer_id=om.customer_id
JOIN ex_payment_management p ON om.order_no=p.order_no
JOIN ex_user_manage u ON om.user_id=u.user_id 
JOIN ex_product_manage pm 
ON od.product_id=pm.product_id 
WHERE om.order_no=$order_no");


        return @$query->result_array();
    }
    function view_confirmed_order_list_by_order_no($order_no)
    {
        $query = $this->db->query("SELECT om.*,od.*,c.*,u.*,pm.*
FROM `ex_order_manage` om 
JOIN order_details od ON om.order_no = od.order_number
JOIN ex_customer_info c ON c.customer_id=om.customer_id
JOIN ex_user_manage u ON om.user_id=u.user_id 
JOIN ex_product_manage pm 
ON od.product_id=pm.product_id 
WHERE om.order_no=$order_no");


        return @$query->result_array();
    }

    function view_indevidual_order_details_by_order_no($order_no)
    {
        $query = $this->db->query("SELECT om.*,u.*
FROM `ex_order_manage` om 
JOIN ex_user_manage u ON om.user_id=u.user_id WHERE om.order_no=$order_no");


        return @$query->result_array();
    }

    function get_customer_by_order_no($order_no)
    {
        $query = $this->db->query("SELECT om.*,c.* FROM `ex_order_manage` om JOIN ex_customer_info c ON c.customer_id=om.customer_id WHERE om.order_no=$order_no");


        return @$query->result_array()[0];

    }

    function total_order()
    {
        return $this->db->get('ex_order_manage')->num_rows();

    }

    function pending_order()
    {
        $this->db->select('*');
        $this->db->from('ex_order_manage');
        $this->db->where('order_status', '0');
        $query = $this->db->get()->num_rows();
        return $query;


    }

}
