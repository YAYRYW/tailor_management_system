<?php


class Ex_role_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get ex_role by role_id
     */
    function get_ex_role($role_id)
    {
        return $this->db->get_where('ex_role', array('role_id' => $role_id))->row_array();
    }

    /*
     * Get all ex_role
     */
    function get_all_ex_role()
    {
        return $this->db->get('ex_role')->result_array();
    }

    /*
     * function to add new ex_role
     */
    function add_ex_role($params)
    {
        $this->db->insert('ex_role', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update ex_role
     */
    function update_ex_role($role_id, $params)
    {
        $this->db->where('role_id', $role_id);
        $response = $this->db->update('ex_role', $params);
        if ($response) {
            return "ex_role updated successfully";
        } else {
            return "Error occuring while updating ex_role";
        }
    }

    /*
     * function to delete ex_role
     */
    function delete_ex_role($role_id)
    {
        $response = $this->db->delete('ex_role', array('role_id' => $role_id));
        if ($response) {
            return "ex_role deleted successfully";
        } else {
            return "Error occuring while deleting ex_role";
        }
    }
}
