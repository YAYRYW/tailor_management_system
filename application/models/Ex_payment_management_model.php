<?php


class Ex_payment_management_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get ex_payment_management by id
     */
    function get_ex_payment_management($id)
    {
        return $this->db->get_where('ex_payment_management', array('id' => $id))->row_array();
    }

    /*
     * Get all ex_payment_management
     */
    function get_all_ex_payment_management()
    {
        return $this->db->get('ex_payment_management')->result_array();
    }

    /*
     * function to add new ex_payment_management
     */
    function add_ex_payment_management($params)
    {
        $this->db->insert('ex_payment_management', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update ex_payment_management
     */
    function update_ex_payment_management($id, $params)
    {
        $this->db->where('id', $id);
        $response = $this->db->update('ex_payment_management', $params);
        if ($response) {
            return "ex_payment_management updated successfully";
        } else {
            return "Error occuring while updating ex_payment_management";
        }
    }

    /*
     * function to delete ex_payment_management
     */
    function delete_ex_payment_management($id)
    {
        $response = $this->db->delete('ex_payment_management', array('id' => $id));
        if ($response) {
            return "ex_payment_management deleted successfully";
        } else {
            return "Error occuring while deleting ex_payment_management";
        }
    }
}
