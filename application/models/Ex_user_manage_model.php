<?php


class Ex_user_manage_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get ex_user_manage by user_id
     */
    function get_ex_user_manage($user_id)
    {
        return $this->db->get_where('ex_user_manage', array('user_id' => $user_id))->row_array();
    }

    /*
     * Get all ex_user_manage
     */
    function get_all_ex_user_manage()
    {

        $this->db->select('*');
        $this->db->from('ex_user_manage');
        $this->db->join('ex_role', 'ex_role.role_id = ex_user_manage.user_role_id');
        $query = $this->db->get()->result_array();
        return $query;
    }

    function get_master()
    {
        $this->db->select('*');
        $this->db->from('ex_user_manage');
        $this->db->join('ex_role', 'ex_role.role_id = ex_user_manage.user_role_id');
        $this->db->where('user_role_id', "3");
        $query = $this->db->get()->result_array();
        return $query;
    }


    /*
     * function to add new ex_user_manage
     */
    function add_ex_user_manage($params)
    {
        $this->db->insert('ex_user_manage', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update ex_user_manage
     */
    function update_ex_user_manage($user_id, $params)
    {
        $this->db->where('user_id', $user_id);
        $response = $this->db->update('ex_user_manage', $params);
        if ($response) {
            return "ex_user_manage updated successfully";
        } else {
            return "Error occuring while updating ex_user_manage";
        }
    }

    /*
     * function to delete ex_user_manage
     */
    function delete_ex_user_manage($user_id)
    {
        $response = $this->db->delete('ex_user_manage', array('user_id' => $user_id));
        if ($response) {
            return "ex_user_manage deleted successfully";
        } else {
            return "Error occuring while deleting ex_user_manage";
        }
    }

    public function login_check($user_name, $password)
    {


        $this->db->select('*');
        $this->db->from('ex_user_manage');
        $this->db->where('user_name', $user_name);
        $this->db->where('user_password', $password);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        }
    }

    function checkUser($user_name)
    {
        $Info = $this->db->query("SELECT `user_name` FROM `ex_user_manage`
        WHERE `user_name` = '$user_name' LIMIT 1");

        return $Info->num_rows();
    }

    function pass_check($password)
    {
        $this->db->select('*');
        $this->db->from('ex_user_manage');
        $this->db->where('user_password', $password);
        $query = $this->db->get();


        return $query->num_rows();

    }



}
